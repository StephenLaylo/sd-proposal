/**
    @author:            Jen Tan & Stephen Laylo
    @company:           J4RVIS
    @description:       Trigger for Project__c
**/
trigger ProjectTrigger on Project__c (before insert, before update, after insert, after update, after delete) {

    if (Trigger.isBefore) {    
        if (Trigger.isInsert) {
            ProjectTriggerHandler.doBeforeInsert(Trigger.new);
        } else if (Trigger.isUpdate) {
            ProjectTriggerHandler.doBeforeUpdate(Trigger.new);
        }

    } else if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            ProjectTriggerHandler.doAfterInsert(Trigger.new);
        } else if (Trigger.isUpdate) {
            ProjectTriggerHandler.doAfterUpdate(Trigger.new, Trigger.oldMap);
            ChangeHistoryUtility.projectChangeHistoryTracker(Trigger.oldMap, Trigger.newMap);
        } else if (Trigger.isDelete) {
            ProjectTriggerHandler.doAfterDelete(Trigger.new);
        }
    }

}