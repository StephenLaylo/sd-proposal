/**
    @author:            Jen Tan
    @company:           J4RVIS
    @description:       Utility Class for Project__c
**/
public without sharing class ProjectUtils {

    /**
        @author:            Jen Tan
        @company:           J4RVIS
        @description:       Updates Proposal Name based on the Proposed Projects
        @param:             newProjects      -   List of new Proposed Projects
    **/
    public static void updateProposalName(List<Project__c> newProjects) {

        Set<Id> proposalIds = new Set<Id>();

        for (Project__c project : newProjects) {
            proposalIds.add(project.Proposal__c);
        }

        List<Project__c> projects = [
            SELECT Id, 
                   Project_Type__c, 
                   Proposal__c,
                   Proposal__r.Proposer_s_First_Name__c, 
                   Proposal__r.Proposer_Last_Name__c 
            FROM Project__c 
            WHERE Proposal__c IN :proposalIds AND 
                  Proposal__c != null
        ];

        // Mapping Proposed Projects by Proposal Id
        Map<Id, List<Project__c>> projectsByProposalMap = new Map<Id, List<Project__c>>();
        for (Project__c project : projects) {
            if (projectsByProposalMap.containsKey(project.Proposal__c)) {
                projectsByProposalMap.get(project.Proposal__c).add(project);

            } else {
                projectsByProposalMap.put(project.Proposal__c, new List<Project__c> {project});
            }
        }

        List<Proposal__c> proposalsToUpdate = new List<Proposal__c>();
        
        // Updating Proposal Name
        for (Id proposalId : projectsByProposalMap.keySet()) {

            List<String> nameList = new List<String>();            
            String proposersFirstName = projectsByProposalMap.get(proposalId)[0].Proposal__r.Proposer_s_First_Name__c;

            if (String.isNotBlank(proposersFirstName)) {
                nameList.add(proposersFirstName);
            }

            String proposersLastName = projectsByProposalMap.get(proposalId)[0].Proposal__r.Proposer_Last_Name__c;
            
            if (String.isNotBlank(proposersLastName)) {
                nameList.add(proposersLastName);
            }
            
            String proposersName = String.join(nameList, ' ');
            String generatedProposalName = generateProposalName(proposersName, projectsByProposalMap.get(proposalId));

            generatedProposalName = generatedProposalName.length() > 80 ? generatedProposalName.substring(0, 80) : generatedProposalName;

            proposalsToUpdate.add(new Proposal__c(Id = proposalId, Name = generatedProposalName));
        }

        if (!proposalsToUpdate.isEmpty()) {
            update proposalsToUpdate;
        }
    }
    
    /**
        @author:            Jen Tan
        @company:           J4RVIS
        @description:       Generates Proposal Name based on the Proposer's Name and related Proposed Projects 
                            (ex. Peter Woodward, 3 x New 4 x modified)
        @param:             proposersName           -   Name of the Proposer
        @param:             projects        -   List of Proposed Project under Proposal
    **/
    public static String generateProposalName(String proposersName, List<Project__c> projects) {
                
        List<String> finalNameList = new List<String>();
        if (String.isNotBlank(proposersName)) {
            finalNameList.add(proposersName);
        }

        if (!projects.isEmpty()) {        
            Map<String, List<Project__c>> projectsMap = new Map<String, List<Project__c>>();

            for (Project__c project : projects) {
                if (projectsMap.containsKey(project.Project_Type__c)) {
                    projectsMap.get(project.Project_Type__c).add(project);

                } else {
                    projectsMap.put(project.Project_Type__c, new List<Project__c> { project });
                }
            }

            List<String> proposalNameList = new List<String>();

            for (String projectType : projectsMap.keySet()) {
                proposalNameList.add(projectsMap.get(projectType).size() + ' x ' + projectType);
            }

            finalNameList.add(String.join(proposalNameList, ' '));
        }

        return String.join(finalNameList, ', ');
    }

    /**
        @author:            Jen Tan
        @company:           J4RVIS
        @description:       creates Case_Relationships__c record that links Project and Publication
        @param:             newProjects            -   new Project records
    **/
    public static void createPublicationRelationship(List<Project__c> newProjects) {
        List<Case_Relationships__c> relationships = new List<Case_Relationships__c>();

        for (Project__c project : newProjects) {
            Id publicationId = null;

            if (Constants.PROJECT_TYPE_REVISED_TEXT_AMENDMENT.equals(project.Project_Type__c)) {
                publicationId = project.Existing_Publication_to_revise_amend__c;

            } else if (Constants.PROJECT_TYPE_INTERNATIONAL_ADOPTION_IDENTICAL.equals(project.Project_Type__c) ||
                Constants.PROJECT_TYPE_INTERNATIONAL_ADOPTION_MODIFIED.equals(project.Project_Type__c)) {
                publicationId = project.International_Publication_to_adopt__c;
            }

            if (publicationId != null) {
                relationships.add(
                    new Case_Relationships__c(
                        Project__c = project.Id, 
                        Publication__c = publicationId, 
                        Relationship_Parent_Type__c = Constants.RELATIONSHIP_PARENT_TYPE_PUBLICATION
                    )
                );
            }
        }

        if (!relationships.isEmpty()) {
            insert relationships;
        }
    }

    /**
        @author:            Jen Tan
        @company:           J4RVIS
        @description:       deletes the previous Case_Relationships__c record, then creates a new Case_Relationships__c 
                                record that will link the newly selected Publication to the Project
        @param:             newProjects            -   new Project records
        @param:             oldProjects            -   old Project records
    **/
    public static void updatePublicationRelationship(List<Project__c> newProjects, Map<Id, Project__c> oldProjects) {
        Map<Id, Id> projectPublicationMapToDelete = new Map<Id, Id>();

        //Project-Publication mapping for the creation of new Relationships records
        Map<Id, Id> projectPublicationMap = new Map<Id, Id>();

        for (Project__c project : newProjects) {
            if (Constants.PROJECT_TYPE_REVISED_TEXT_AMENDMENT.equals(project.Project_Type__c)) {
                if (project.Existing_Publication_to_revise_amend__c != oldProjects.get(project.Id).Existing_Publication_to_revise_amend__c) {
                    if (project.Existing_Publication_to_revise_amend__c != null) {
                        projectPublicationMap.put(project.Id, project.Existing_Publication_to_revise_amend__c);
                    }

                    projectPublicationMapToDelete.put(project.Id, oldProjects.get(project.Id).Existing_Publication_to_revise_amend__c);
                }

            } else if (Constants.PROJECT_TYPE_INTERNATIONAL_ADOPTION_IDENTICAL.equals(project.Project_Type__c) || Constants.PROJECT_TYPE_INTERNATIONAL_ADOPTION_MODIFIED.equals(project.Project_Type__c)) {
                if (project.International_Publication_to_adopt__c != oldProjects.get(project.Id).International_Publication_to_adopt__c) {
                    if (project.International_Publication_to_adopt__c != null) {
                        projectPublicationMap.put(project.Id, project.International_Publication_to_adopt__c);
                    }

                    projectPublicationMapToDelete.put(project.Id, oldProjects.get(project.Id).International_Publication_to_adopt__c);
                }
            }
        }

        // Delete previous relationship
        List<Case_Relationships__c> existingRelationships = [
            SELECT Id, 
                   Project__c, 
                   Publication__c 
            FROM Case_Relationships__c 
            WHERE Publication__c IN: projectPublicationMapToDelete.values() AND 
                  Project__c IN: projectPublicationMapToDelete.keySet()
        ];

        List<Case_Relationships__c> relationshipsToDelete = new List<Case_Relationships__c>();

        for (Case_Relationships__c relationship : existingRelationships) {
            if (projectPublicationMapToDelete.containsKey(relationship.Project__c) && projectPublicationMapToDelete.get(relationship.Project__c) == relationship.Publication__c) {
                relationshipsToDelete.add(relationship);
            }
        }

        if (!relationshipsToDelete.isEmpty()) {
            delete relationshipsToDelete;
        }

        // Create new Project-Publication relationships
        if (!projectPublicationMap.isEmpty()) {
            List<Case_Relationships__c> relationshipsToCreate = new List<Case_Relationships__c>();

            for (Id projectId : projectPublicationMap.keySet()) {
                relationshipsToCreate.add(
                    new Case_Relationships__c(
                        Project__c = projectId, 
                        Publication__c = projectPublicationMap.get(projectId), 
                        Relationship_Parent_Type__c = Constants.RELATIONSHIP_PARENT_TYPE_PUBLICATION
                    )
                );
            }

            insert relationshipsToCreate;
        }
    }

}