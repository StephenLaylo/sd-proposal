public class Constants {

    //Account
    public static final String ACCOUNT_RECORDTYPE_DEVELOPERNAME_ORGANISATION = 'Organisation';
    public static final String ACCOUNT_RECORDTYPE_DEVELOPERNAME_NATIONAL_COMMITTEE = 'National_Committee';
    public static final String ACCOUNT_RECORDTYPE_DEVELOPERNAME_INTERNATIONAL_COMMITTEE = 'International_Committee';
    public static final String ACCOUNT_RECORDTYPE_DEVELOPERNAME_INTERNATIONAL_Body = 'International_Body';
    public static final String ACCOUNT_RECORDTYPE_DEVELOPERNAME_Household = 'Household';

    public static final String ACCOUNT_SA_STATUS_ACTIVE = 'Active';
    public static final String ACCOUNT_COMMITTEE_STATUS_ACTIVE = 'Active';
    public static final String ACCOUNT_COMMITTEE_TYPE_SC = 'SC';
    public static final String ACCOUNT_COMMITTEE_TYPE_WG = 'WG';
    public static final String ACCOUNT_COMMITTEE_TYPE_TC = 'TC';

    //Generate XML
    public static final String GENERATEXML_RECORDTYPE_FIELD_ORGANISATION = 'Organisiation';
    public static final String GENERATEXML_RECORDTYPE_FIELD_NATIONAL_COMMITTEE = 'National Committee';

    //Committee Role
    public static final String COMMITTEEROLE_ROLE_STATUS_CURRENT = 'Current';
    public static final String COMMITTEEROLE_ROLE_STATUS_FORMER = 'Former';
    public static final String COMMITTEEROLE_ROLE_STATUS_AWAITING_ASSIGNMENT = 'Awaiting Assignment';
    
    //Contact
    public static final String CONTACT_SA_STATUS_ACTIVE = 'Active';

    //User
    public static final String GROUP_DEVELOPERNAME_SYSTEM_USER_QUEUE = 'System_User_Queue';
    public static final String GROUP_TYPE_QUEUE = 'Queue';
    public static final String USER_LICENSE_SF = 'Salesforce';
    public static final String USER_LICENSE_SF_PLATFORM = 'Salesforce Platform';

    //Logs
    public static final String LOG_LEVEL_ERROR = 'Error';
    public static final String LOG_LEVEL_DEBUG = 'Debug';
    public static final String LOG_LEVEL_INFO = 'Info';
    public static final String LOG_LEVEL_WARN = 'Warning';
    public static final String LOG_UTIL_RESPONSE_SUCCESS = 'Success';
    public static final String LOG_UTIL_RESPONSE_INVALID_REQUEST = 'Invalid Request';
    public static final String LOG_UTIL_RESPONSE_UNAUTHORIZED = 'Unauthorized';
    public static final String LOG_UTIL_RESPONSE_INTERNAL_SERVER_ERROR = 'Internal Server Error';
    public static final String LOG_UTIL_RESPONSE_UNEXPECTED_SYSTEM_ERROR = 'Unexpected system error or system timeout';
    public static final String LOG_UTIL_RESPONSE_UNKNOWN_ERROR = 'Unknown Error';
    public static final String LOG_UTIL_RESPONSE_EXCEPTION = 'Exception';

    //Proposal
    public static final String PROPOSAL_DEFAULT_REC_TYPE = Schema.SObjectType.Proposal__c.getRecordTypeInfosByName().get('Default').getRecordTypeId();

    //Proposal Type
    public static final String PROJECT_PROPOSAL_TYPE_CREATE_NEW = 'Create a New Publication';
    public static final String PROJECT_PROPOSAL_TYPE_REVISE_EXISTING = 'Revise/amend existing publications';
    public static final String PROJECT_PROPOSAL_TYPE_ADOPT_INTERNATIONAL = 'Adopt international publications';

    //Proposal Status
    public static final String PROPOSAL_STATUS_DRAFT = 'Draft';
    public static final String PROPOSAL_STATUS_NEW = 'New';
    public static final String PROPOSAL_STATUS_IN_DEVELOPMENT = 'In Development';
    public static final String PROPOSAL_STATUS_IN_CONSULTATION = 'In Consultation';
    public static final String PROPOSAL_STATUS_CONSULTATION_CLOSED = 'Consultation Closed';
    public static final String PROPOSAL_STATUS_HSE_REVIEW = 'HSE Review';
    public static final String PROPOSAL_STATUS_PMG_REVIEW = 'PMG Review';
    public static final String PROPOSAL_STATUS_SDAC_REVIEW = 'SDAC Review';
    public static final String PROPOSAL_STATUS_AWAITING_RESOURCES = 'Awaiting Resources';
    public static final String PROPOSAL_STATUS_APPROVED = 'Approved';
    public static final String PROPOSAL_STATUS_COMPLETED = 'Completed';
    public static final String PROPOSAL_STATUS_NOT_PROGRESSED = 'Not Progressed';

    //Proposal Stage Filter
    public static final String PROPOSAL_STAGE_FILTER_ALL = 'All';
    public static final String PROPOSAL_STAGE_FILTER_DRAFT = 'Draft';
    public static final String PROPOSAL_STAGE_FILTER_SUBMITTED = 'Submitted';
    public static final String PROPOSAL_STAGE_FILTER_CONSULTING = 'Consulting';
    public static final String PROPOSAL_STAGE_FILTER_FOR_APPROVAL = 'For Approval';
    public static final String PROPOSAL_STAGE_FILTER_FINISHED = 'Finished';

    //Project Type
    public static final String PROJECT_TYPE_NEW = 'New';
    public static final String PROJECT_TYPE_REVISED_TEXT_AMENDMENT = 'Revised text amendment';
    public static final String PROJECT_TYPE_CORRECTION_AMENDMENT = 'Correction amendment';
    public static final String PROJECT_TYPE_REVISION = 'Revision';
    public static final String PROJECT_TYPE_INTERNATIONAL_ADOPTION_IDENTICAL = 'International adoption identical';
    public static final String PROJECT_TYPE_INTERNATIONAL_ADOPTION_MODIFIED = 'International adoption modified';

    //Project Type Choices
    public static final String PROJECT_TYPE_CHOICES_IDENTICAL = 'Identical';
    public static final String PROJECT_TYPE_CHOICES_MODIFIED = 'Modified';

    //Project Status
    public static final String PROJECT_STATUS_PROPOSAL = 'Proposal';

    //Relationship Parent Type
    public static final String RELATIONSHIP_PARENT_TYPE_PROJECT = 'Project';
    public static final String RELATIONSHIP_PARENT_TYPE_PUBLICATION = 'Publication';

    @AuraEnabled
    public static Object getAllConstants() {
        Map<String, String> allConstantsMap = new Map<String, String> {
            'PROPOSAL_DEFAULT_REC_TYPE' => PROPOSAL_DEFAULT_REC_TYPE, 
            'PROJECT_PROPOSAL_TYPE_CREATE_NEW' => PROJECT_PROPOSAL_TYPE_CREATE_NEW, 
            'PROJECT_PROPOSAL_TYPE_REVISE_EXISTING' => PROJECT_PROPOSAL_TYPE_REVISE_EXISTING, 
            'PROJECT_PROPOSAL_TYPE_ADOPT_INTERNATIONAL' => PROJECT_PROPOSAL_TYPE_ADOPT_INTERNATIONAL, 
            'PROPOSAL_STATUS_DRAFT' => PROPOSAL_STATUS_DRAFT, 
            'PROPOSAL_STATUS_NEW' => PROPOSAL_STATUS_NEW, 
            'PROPOSAL_STATUS_IN_DEVELOPMENT' => PROPOSAL_STATUS_IN_DEVELOPMENT, 
            'PROPOSAL_STATUS_IN_CONSULTATION' => PROPOSAL_STATUS_IN_CONSULTATION, 
            'PROPOSAL_STATUS_CONSULTATION_CLOSED' => PROPOSAL_STATUS_CONSULTATION_CLOSED, 
            'PROPOSAL_STATUS_HSE_REVIEW' => PROPOSAL_STATUS_HSE_REVIEW, 
            'PROPOSAL_STATUS_PMG_REVIEW' => PROPOSAL_STATUS_PMG_REVIEW, 
            'PROPOSAL_STATUS_SDAC_REVIEW' => PROPOSAL_STATUS_SDAC_REVIEW, 
            'PROPOSAL_STATUS_AWAITING_RESOURCES' => PROPOSAL_STATUS_AWAITING_RESOURCES, 
            'PROPOSAL_STATUS_APPROVED' => PROPOSAL_STATUS_APPROVED, 
            'PROPOSAL_STATUS_COMPLETED' => PROPOSAL_STATUS_COMPLETED, 
            'PROPOSAL_STATUS_NOT_PROGRESSED' => PROPOSAL_STATUS_NOT_PROGRESSED, 
            'PROPOSAL_STAGE_FILTER_ALL' => PROPOSAL_STAGE_FILTER_ALL, 
            'PROPOSAL_STAGE_FILTER_DRAFT' => PROPOSAL_STAGE_FILTER_DRAFT, 
            'PROPOSAL_STAGE_FILTER_CONSULTING' => PROPOSAL_STAGE_FILTER_CONSULTING, 
            'PROPOSAL_STAGE_FILTER_SUBMITTED' => PROPOSAL_STAGE_FILTER_SUBMITTED, 
            'PROPOSAL_STAGE_FILTER_FOR_APPROVAL' => PROPOSAL_STAGE_FILTER_FOR_APPROVAL, 
            'PROPOSAL_STAGE_FILTER_FINISHED' => PROPOSAL_STAGE_FILTER_FINISHED, 
            'PROJECT_TYPE_NEW' => PROJECT_TYPE_NEW, 
            'PROJECT_TYPE_REVISED_TEXT_AMENDMENT' => PROJECT_TYPE_REVISED_TEXT_AMENDMENT, 
            'PROJECT_TYPE_CORRECTION_AMENDMENT' => PROJECT_TYPE_CORRECTION_AMENDMENT, 
            'PROJECT_TYPE_REVISION' => PROJECT_TYPE_REVISION, 
            'PROJECT_TYPE_INTERNATIONAL_ADOPTION_IDENTICAL' => PROJECT_TYPE_INTERNATIONAL_ADOPTION_IDENTICAL, 
            'PROJECT_TYPE_INTERNATIONAL_ADOPTION_MODIFIED' => PROJECT_TYPE_INTERNATIONAL_ADOPTION_MODIFIED, 
            'PROJECT_TYPE_CHOICES_IDENTICAL' => PROJECT_TYPE_CHOICES_IDENTICAL, 
            'PROJECT_TYPE_CHOICES_MODIFIED' => PROJECT_TYPE_CHOICES_MODIFIED, 
            'PROJECT_STATUS_PROPOSAL' => PROJECT_STATUS_PROPOSAL, 
            'RELATIONSHIP_PARENT_TYPE_PROJECT' => RELATIONSHIP_PARENT_TYPE_PROJECT, 
            'RELATIONSHIP_PARENT_TYPE_PUBLICATION' => RELATIONSHIP_PARENT_TYPE_PUBLICATION
        };

        return JSON.deserializeUntyped(JSON.serialize(allConstantsMap));
    }

}