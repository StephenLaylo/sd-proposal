/**
    @author:            Stephen Laylo
    @company:           J4RVIS
    @description:       Utility class that should hold all methods that are reused across the application
*/
public without sharing class CommonUtils {
    
    /**
        @author:            Stephen Laylo
        @company:           J4RVIS
        @description:       Method to retrieve Standard Picklist Values
        @params:            objectName         -    name of the object
        @params:            fieldName          -    name of the field
    */
    @AuraEnabled
    public static Map<String, String> getPicklistValues(String objectName, String fieldName) {
        List<Schema.PicklistEntry> ple = Schema.getGlobalDescribe().get(objectName).newSObject().getSObjectType().getDescribe().fields.getMap().get(fieldName).getDescribe().getPickListValues();
        Map<String, String> picklistValuesMap = new Map<String, String>();

        for (Schema.PicklistEntry pickListVal : ple) {
            picklistValuesMap.put(pickListVal.getValue(), pickListVal.getLabel());
        } 

        return picklistValuesMap;
    }

    /**
        @author:            Stephen Laylo
        @company:           J4RVIS
        @description:       Method to retrieve the Base URL in External Form
    */
    @AuraEnabled
    public static String getSalesforceBaseUrlExternalForm() {
        return URL.getSalesforceBaseUrl().toExternalForm();
    }

    /**
        @author:            Stephen Laylo
        @company:           J4RVIS
        @description:       Method to fire an assignment rule based on name and returns the DML Options
        @params:            assignmentRuleName         -    name of the assignment rule
    */
    @AuraEnabled 
    public static Database.DMLOptions fireAssignmentRule(String assignmentRuleName) {
        //Fetching the assignment rules on case
        List<AssignmentRule> assignmentRules = [
            SELECT Id 
            FROM AssignmentRule 
            WHERE Name = :assignmentRuleName AND 
                  Active = true 
            LIMIT 1
        ];

        if (!assignmentRules.isEmpty()) {
            //Creating the DMLOptions for "Assign using active assignment rules" checkbox
            Database.DMLOptions dmlOpts = new Database.DMLOptions();
            dmlOpts.assignmentRuleHeader.assignmentRuleId = assignmentRules[0].Id;

            return dmlOpts;
        }
        
        return null;
    }

    /**
        @author:            Stephen Laylo
        @company:           J4RVIS
        @description:       Method to convert bytes to kilobytes, megabytes or gigabytes
        @params:            value         -    value of bytes
    */
    public static String getFileSizeToString(Long value) {
        /* string representation if a file's size, such as 2 KB, 4.1 MB, etc */
        if (value < 1024) {
            return String.valueOf(value) + ' Bytes';

        } else if (value >= 1024 && value < (1024*1024)) {
            //KB
            Decimal kb = Decimal.valueOf(value);
            kb = kb.divide(1024, 2);

            return String.valueOf(kb) + ' KB';

        } else if (value >= (1024 * 1024) && value < (1024 * 1024 * 1024)) {
            //MB
            Decimal mb = Decimal.valueOf(value);
            mb = mb.divide((1024 * 1024), 2);

            return String.valueOf(mb) + ' MB';

        } else {
            //GB
            Decimal gb = Decimal.valueOf(value);
            gb = gb.divide((1024 * 1024 * 1024), 2);
            
            return String.valueOf(gb) + ' GB';
        }    
    }

}