/**
    @author:            Jen Tan & Stephen Laylo
    @company:           J4RVIS
    @description:       Controller class for Proposal Form Lightning Web Component; Handles all the transaction related to that component
**/
public with sharing class ProposalFormController {

    @AuraEnabled
    public static Map<Id, List<Project__c>> upsertProposalAndProjects(Proposal__c proposalRecord, List<Object> projectsWithPublications) {
        Savepoint sp = Database.setSavepoint();

        System.debug(LoggingLevel.DEBUG, '~~ projectsWithPublications : ' + projectsWithPublications);

        List<Project__c> projects = new List<Project__c>();
        List<ProjectWithPublications> parsedProjectsWithPublications = (List<ProjectWithPublications>) JSON.deserialize(JSON.serialize(projectsWithPublications), List<ProjectWithPublications>.class);
        Map<Project__c, List<String>> projectsMap = new Map<Project__c, List<String>>();

        try {
            for (ProjectWithPublications parsedProjectWithPublications : parsedProjectsWithPublications) {
                projects.add(parsedProjectWithPublications.projectRecord);
            }

            proposalRecord.RecordTypeId = Constants.PROPOSAL_DEFAULT_REC_TYPE;

            upsert proposalRecord;

            for (Project__c projectRecord : projects) {
                projectRecord.Proposal__c = proposalRecord.Id;
            }
    
            upsert projects;

            projectsMap = new Map<Project__c, List<String>>();

            for (ProjectWithPublications parsedProjectsWithPublication : parsedProjectsWithPublications) {
                // projectsMap.put(parsedProjectsWithPublication.projectRecord, parsedProjectsWithPublication.selectedPublications);
                List<String> publicationIds = new List<String>();
                for (PublicationData publication : parsedProjectsWithPublication.selectedPublications) {
                    publicationIds.add(publication.value);
                }
                projectsMap.put(parsedProjectsWithPublication.projectRecord, publicationIds);
            }

            updateProjectPublications(projectsMap);
            
        } catch (Exception ex) {
            Database.rollback(sp);

            LogUtil.getInstance().addLog(
                'ProposalFormController',
                'upsertProposalAndProjects',
                ex,
                ex.getMessage(),
                '',
                'Trying to upsert a Proposal and multiple Project records.' + '\n' +
                'proposalRecord = ' + JSON.serialize(proposalRecord) + '\n' +
                'projectsWithPublications = ' + JSON.serialize(projectsWithPublications) + '\n' +
                'projectsMap = ' + JSON.serialize(projectsMap),
                '',
                ex.getStackTraceString(),
                '',
                Constants.LOG_LEVEL_ERROR
            ).createLogs(true);

            return null;
        }

        return new Map<Id, List<Project__c>> {
            proposalRecord.Id => projects
        };
    }

    /**
        @author:            Jen Tan & Stephen Laylo
        @company:           J4RVIS
        @description:       Updates Project-Publication records. Inserts Proposal-Publication record for new Publication,
                            deletes Project-Publication record for deselected Publication
        @param:             projectId                   -   Id of the related Proposal record
        @param:             publicationIds              -   contains the selected Publication Ids
    **/
    private static void updateProjectPublications(Map<Project__c, List<String>> projectsMap) {
        Set<Id> projectIds = new Set<Id>();

        for (Project__c projectRecord : projectsMap.keySet()) {
            projectIds.add(projectRecord.Id);
        }

        if (projectIds != null && !projectIds.isEmpty()) {
            List<Case_Relationships__c> existingProjectPublications = [
                SELECT Id, 
                       Name, 
                       Publication__c 
                FROM Case_Relationships__c 
                WHERE Project__c IN :projectIds AND 
                      Relationship_Parent_Type__c = :Constants.RELATIONSHIP_PARENT_TYPE_PROJECT
                ORDER BY Project__r.CreatedDate, CreatedDate
            ];

            if (!existingProjectPublications.isEmpty()) {
                delete existingProjectPublications;
            }

            List<Case_Relationships__c> projectPublications = new List<Case_Relationships__c>();

            for (Project__c projectRecord : projectsMap.keySet()) {
                if (projectRecord != null && projectsMap.containsKey(projectRecord)) {
                    List<String> publicationIds = projectsMap.get(projectRecord);
                    
                    if (publicationIds != null) {
                        for (Id publicationId : publicationIds) {
                            if (publicationId != null) {
    
                                //create Project-Publication record for new Publications
                                projectPublications.add(
                                    new Case_Relationships__c(
                                        Project__c = projectRecord.Id, 
                                        Publication__c = publicationId,
                                        Relationship_Parent_Type__c = Constants.RELATIONSHIP_PARENT_TYPE_PROJECT
                                    )
                                );
                            }
                        }
                    }
                }
            }

            if (!projectPublications.isEmpty()) {
                insert projectPublications;
            }
        }
    }

    /**
        @author:            Stephen Laylo
        @company:           J4RVIS
        @description:       Returns list of Proposals by Proposal Status that the current user has access
        @param:             proposalStatus                   -   status of the Proposal
        @param:             searchText                       -   search text/keyword
    **/
    @AuraEnabled
    public static List<Proposal__c> getProposalsBySearchText(List<String> proposalStatus, String searchText, Integer pageNumber, Integer rowsPerPage, String sortedField, String sortDirection) {
        if (searchText == null) {
            searchText = '';
        }

        List<Proposal__c> proposals = Database.query(
            'SELECT Id, ' + 
                   'Name, ' + 
                   'Proposer_Contact__r.Name, ' + 
                   'Proposal_Number__c, ' + 
                   'Proposal_Status__c, ' + 
                   'Submitted_Date__c, ' + 
                   'Consultation_Start_Date__c, ' + 
                   'Consultation_End_Date__c, ' + 
                   'Approved_Date__c, ' + 
                   'Complete_Date__c, ' + 
                   'Net_Benefit_Public_and_health_safety__c, ' + 
                   'Net_Benefit_Social_and_community_impact__c, ' + 
                   'Net_Benefit_Environmental_Impact__c, ' + 
                   'Net_Benefit_Competition__c, ' + 
                   'Net_Benefit_Economic_impact__c, ' + 
                   'Who_has_been_consulted__c ' + 
            'FROM Proposal__c ' + 
            'WHERE Proposal_Status__c IN :proposalStatus ' + (searchText.trim() != '' ? +'AND ' + 
                  '(' +
                        'Name LIKE \'%' + String.escapeSingleQuotes(searchText) + '%\' OR ' + 
                        'Proposal_Number__c LIKE \'%' + String.escapeSingleQuotes(searchText) + '%\'' + 
                   ')' : '') +
            'ORDER BY ' + sortedField + ' ' + sortDirection + ' ' +
            'LIMIT ' + rowsPerPage + ' ' + (pageNumber > 1 ? 
            'OFFSET ' + (rowsPerPage * (pageNumber - 1)) : '')
        );

        return proposals;
    }

    @AuraEnabled
    public static Proposal getProposalRecord(Id proposalId) {
        System.debug(LoggingLevel.DEBUG, '~~ proposalId: ' + proposalId);

        List<Project__c> projects = [
            SELECT Id, 
                    Name, 
                    Project_Type__c, 
                    Product_Type__c, 
                    Title__c, 
                    Designation__c, 
                    What_problem_is_this_proposal_designed_t__c, 
                    What_is_the_summarised_scope__c, 
                    Issues_with_Current_edition__c, 
                    What_evidence_exist__c, 
                    What_is_your_Proposed_Solution__c, 
                    Existing_Publication_to_revise_amend__c, 
                    Existing_Publication_to_revise_amend__r.Title__c, 
                    Existing_Publication_to_revise_amend__r.Name, 
                    Why_does_standard_need_to_be_adopted__c, 
                    Reasons_for_non_identical_adoption__c, 
                    What_are_your_Proposed_Variations__c, 
                    International_Publication_to_adopt__c, 
                    International_Publication_to_adopt__r.Title__c, 
                    International_Publication_to_adopt__r.Name,                 
                    Proposal__r.Id, 
                    Proposal__r.Name, 
                    Proposal__r.Proposer_Contact__r.Name, 
                    Proposal__r.Proposal_Number__c, 
                    Proposal__r.Proposal_Status__c, 
                    Proposal__r.Submitted_Date__c, 
                    Proposal__r.Consultation_Start_Date__c, 
                    Proposal__r.Consultation_End_Date__c, 
                    Proposal__r.Approved_Date__c, 
                    Proposal__r.Complete_Date__c, 
                    Proposal__r.Net_Benefit_Public_and_health_safety__c, 
                    Proposal__r.Net_Benefit_Social_and_community_impact__c, 
                    Proposal__r.Net_Benefit_Environmental_Impact__c, 
                    Proposal__r.Net_Benefit_Competition__c, 
                    Proposal__r.Net_Benefit_Economic_impact__c, 
                    Proposal__r.Who_has_been_consulted__c,
                    (
                        SELECT Id, 
                               Publication__c,
                               Publication__r.Title__c,
                               Publication__r.Name
                        FROM Case_Relationships__r 
                        WHERE Relationship_Parent_Type__c = :Constants.RELATIONSHIP_PARENT_TYPE_PROJECT
                    )
            FROM Project__c
            WHERE Proposal__c = :proposalId
        ];

        System.debug(LoggingLevel.DEBUG, '~~ projects: ' + projects);

        Proposal proposal = null;

        if (!projects.isEmpty()) {
            proposal = new Proposal();
            proposal.proposalRecord = projects[0].Proposal__r;
            proposal.projects = new List<ProposalFormController.ProjectWithPublications>();

            for (Project__c project : projects) {

                ProjectWithPublications projectWithPublications = new ProjectWithPublications();
                projectWithPublications.projectRecord = extractProject(project);
                
                if (project.Case_Relationships__r != null) {
                    // projectWithPublications.selectedPublications = new List<String>();
                    projectWithPublications.selectedPublications = new List<PublicationData>();
                    

                    for (Case_Relationships__c relationship : project.Case_Relationships__r) {
                        PublicationData publication = new PublicationData();
                        publication.value = relationship.Publication__c;
                        publication.label = relationship.Publication__r.Name + ' • ' + relationship.Publication__r.Title__c;
                        publication.name = relationship.Publication__r.Name;                        
                        // projectWithPublications.selectedPublications.add(relationship.Publication__c);
                        projectWithPublications.selectedPublications.add(publication);
                    }
                }

                proposal.projects.add(projectWithPublications);                                
            }

        }

        System.debug(LoggingLevel.DEBUG, '~~ proposal: ' + proposal);

        return proposal;
    }

    private static Project__c extractProject(Project__c project) {
        return new Project__c(
            Id = project.Id,
            Name = project.Name,
            Project_Type__c = project.Project_Type__c, 
            Product_Type__c = project.Product_Type__c, 
            Title__c = project.Title__c, 
            Designation__c = project.Designation__c, 
            What_problem_is_this_proposal_designed_t__c = project.What_problem_is_this_proposal_designed_t__c, 
            What_is_the_summarised_scope__c = project.What_is_the_summarised_scope__c, 
            Issues_with_Current_edition__c = project.Issues_with_Current_edition__c, 
            What_evidence_exist__c = project.What_evidence_exist__c, 
            What_is_your_Proposed_Solution__c = project.What_is_your_Proposed_Solution__c, 
            Existing_Publication_to_revise_amend__c = project.Existing_Publication_to_revise_amend__c, 
            Why_does_standard_need_to_be_adopted__c = project.Why_does_standard_need_to_be_adopted__c, 
            Reasons_for_non_identical_adoption__c = project.Reasons_for_non_identical_adoption__c, 
            What_are_your_Proposed_Variations__c = project.What_are_your_Proposed_Variations__c, 
            International_Publication_to_adopt__c = project.International_Publication_to_adopt__c
        );
    }

    @TestVisible
    private class Proposal {

        @AuraEnabled
        public Proposal__c proposalRecord { get; set; }

        @AuraEnabled
        public List<ProjectWithPublications> projects { get; set; }

    }

    /**
        @author:            Stephen Laylo
        @company:           J4RVIS
        @description:       Wrapper class that holds Project records with Publication IDs
    **/
    @TestVisible
    private class ProjectWithPublications {

        @AuraEnabled public Project__c projectRecord { get; set; }
        // @AuraEnabled public List<String> selectedPublications { get; set; }
        @AuraEnabled public List<PublicationData> selectedPublications {get; set;}

    }

    private class PublicationData {
        @AuraEnabled public String name {get; set;}
        @AuraEnabled public String value {get; set;}
        @AuraEnabled public String label {get; set;}
    }
}