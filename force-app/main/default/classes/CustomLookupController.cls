/**
    @author:            Stephen Laylo
    @company:           J4RVIS
    @description:       Controller class for Multi Select Lightning Web Component
**/
public without sharing class CustomLookupController {

    /**
        @author:            Stephen Laylo
        @company:           J4RVIS
        @description:       This method retrieves the data from database table. It search input is '*', then retrieve all records
        @param:             objectName              -   Name of the Object for the search
        @param:             fieldAPINames           -   API Name of the fields to be displayed (comma separated)
        @param:             filterFieldAPIName      -   API Name of the field as a filter
        @param:             strInput                -   User's string input
    **/
    @AuraEnabled(cacheable=true)
    public static List<SObjectQueryResult> retrieveRecords(String objectName, String fieldAPINames, String filterFieldAPIName, String strInput) {
        
        List<SObjectQueryResult> lstReturnResult = new List<SObjectQueryResult>();

        if (strInput.equals('*')) {
            strInput = '';
        }

        String str = strInput + '%';
        String strQueryField = '';
        List<String> fieldList = fieldAPINames.split(',');

        //check if Id is already been passed
        if (!fieldList.contains('Id')) {
            fieldList.add('Id');
            strQueryField = String.join(fieldList, ',');

        } else {
            strQueryField = fieldAPINames;
        }

        String strQuery = 'SELECT ' + String.escapeSingleQuotes(strQueryField) 
                        + ' FROM ' 
                        + String.escapeSingleQuotes(objectName) 
                        + ' WHERE ' + filterFieldAPIName + '  LIKE \'' + str + '%\''
                        + ' ORDER BY ' + filterFieldAPIName
                        + ' LIMIT 50';

        List<SObject> lstResult = Database.query(strQuery);

        //create list of records that can be easily be parsable at js controller.
        for (String strField : fieldList) {
            for (SObject sobj : lstResult) {                
                if (strField != 'Id') {
                    SObjectQueryResult result = new SObjectQueryResult();
                    result.recordId = (String) sobj.get('Id');
                    result.recordName = (String) sobj.get(strField);
                    lstReturnResult.add(result);
                }                
            }
        }

        return lstReturnResult;
    }

    /**
        @author:            Jen Tan & Stephen Laylo
        @company:           J4RVIS
        @description:       This method retrieves the data from database table based on the given record Ids.
        @param:             objectName              -   Name of the Object for the search
        @param:             fieldAPINames           -   API Name of the fields to be displayed (comma separated)
        @param:             recordIds               -   Id of the records
    **/
    @AuraEnabled(cacheable=true)
    public static List<SObjectQueryResult> retrieveRecordsByIds(String objectName, String fieldAPINames, List<String> recordIds) {

        //trimming string record Ids to make sure there is no blank spaces then converting type to Id
        Set<Id> ids = new Set<Id>();
        for (String recordId : recordIds) {
            ids.add(Id.valueOf(recordId.trim()));
        }

        List<SObjectQueryResult> lstReturnResult = new List<SObjectQueryResult>();
        
        String strQueryField = '';
        List<String> fieldList = fieldAPINames.split(',');

        //check if Id is already been passed
        if (!fieldList.contains('Id')) {
            fieldList.add('Id');
            strQueryField = String.join(fieldList, ',');
        } else {
            strQueryField = fieldAPINames;
        }

        String strQuery = 'SELECT ' + String.escapeSingleQuotes(strQueryField) 
                        + ' FROM ' 
                        + String.escapeSingleQuotes(objectName) 
                        + ' WHERE Id IN: ids' + 
                        + ' ORDER BY Name';

        List<SObject> lstResult = Database.query(strQuery);

        for (SObject sobj : lstResult) {                
            SObjectQueryResult result = new SObjectQueryResult();
            result.recordId = (String) sobj.get('Id');
            result.recordName = (String) sobj.get('Name');
            result.title = (String) sobj.get('Title__c');
            lstReturnResult.add(result);          
        }
        return lstReturnResult;
    }

    /**
        @author:            Jen Tan 
        @company:           J4RVIS
        @description:       This method retrieves the data from database table based on the search string.
        @param:             objectName              -   Name of the Object for the search
        @param:             fieldAPINames           -   API Name of the fields to be displayed (comma separated)
        @param:             strInput                -   User's search input
    **/
    @AuraEnabled(cacheable=true)
    public static List<SObjectQueryResult> searchForRecords(String objectName, String fieldAPINames, String strInput) {

        String strQuery = 'FIND ' + '\'*' + strInput + '*\'' + ' IN ALL FIELDS RETURNING ' + objectName + '(' + fieldAPINames + ')';

        List<SObjectQueryResult> lstReturnResult = new List<SObjectQueryResult>();

        List<List<SObject>> results = Search.query(strQuery);
        for (List<SObject> resultsList : results) {
            for (SObject result : resultsList) {
                SObjectQueryResult queryResult = new SObjectQueryResult();
                queryResult.recordId = (String) result.get('Id');
                queryResult.recordName = (String) result.get('Name');
                queryResult.title = (String) result.get('Title__c');
                lstReturnResult.add(queryResult);
            }
        }
        return lstReturnResult;
    }
    
    /**
        @author:            Stephen Laylo
        @company:           J4RVIS
        @description:       Wrapper Class for the Query Result
    **/
    public class SObjectQueryResult {
        @AuraEnabled
        public String recordId;

        @AuraEnabled
        public String recordName;

        @AuraEnabled
        public String title;
    }

}