/**
    @author:            Jen Tan & Stephen Laylo
    @company:           J4RVIS
    @description:       Test class for Proposal Form Controller class
*/
@isTest(seeAllData=true)
public class ProposalFormControllerTest {

    // @testSetup
    // private static void makeTestData() {

    // }

    private static testMethod void testHappyPath() {

    }

    private static testMethod void testSadPath() {

    }

    private static testMethod void testProposalRetrieval() {

        Test.startTest();

        List<String> proposalStatus = new List<String>{
            'Draft'
        };

        List<ProposalFormController.Proposal> proposals = ProposalFormController.getProposalsTemp(proposalStatus);

        Test.stopTest();

        System.assert(!proposals.isEmpty());

        for (ProposalFormController.Proposal proposal : proposals) {
            System.debug('~~ START - Proposal Record : ' + proposal.proposalRecord);
            for (ProposalFormController.ProjectWithPublications project : proposal.projects) {
                System.debug('~~ Project : ' + project);
                for (String publicationId : project.selectedPublications) {
                    System.debug('~~ Publications : ' + publicationId);
                }
            }
        }
    }

}