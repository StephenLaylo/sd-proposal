/*************************************************************************************************************
 * @name			ProjectTriggerHandler
 * @author			jade.serrano@asggroup.com.au
 * @description		Handles Project Trigger
**************************************************************************************************************/
public with sharing class ProjectTriggerHandler {

    public static void doAfterInsert(List<Project__c> newProjectsList) {
        ProjectUtils.updateProposalName(newProjectsList);
        ProjectUtils.createPublicationRelationship(newProjectsList);
    }

    public static void doAfterUpdate(List<Project__c> newProjectsList, Map<Id, Project__c> oldProjectsMap) {
        ProjectUtils.updateProposalName(newProjectsList);
        ProjectUtils.updatePublicationRelationship(newProjectsList, oldProjectsMap);
    }

    public static void doAfterDelete(List<Project__c> newProjectsList) {
        ProjectUtils.updateProposalName(newProjectsList);
    }

    public static void doBeforeInsert(List<Project__c> projectList) {
        if (UserInfo.getName() == 'Integration User') {
            System.debug('Integration User');

            Set<Id> committeeCodes = new Set<Id>();
            Set<String> sectorCodes = new Set<String>();
            Set<String> projectManagerEmails = new Set<String>();
            Set<String> programManagerIDs = new Set<String>();
            Set<String> stakeholderEngManagerIDs = new Set<String>();

        for (Project__c p : projectList) {
            if (String.isNotBlank(p.Mulesoft_National_Committee_Code__c)) {
                committeeCodes.add(p.Mulesoft_National_Committee_Code__c);
            }
            
            sectorCodes.add(p.Mulesoft_Sector_Code__c);
            projectManagerEmails.add(p.Mulesoft_Project_Manager_Email__c);
            programManagerIds.add(p.Mulesoft_Program_Manager__c);
            stakeholderEngManagerIDs.add(p.Mulesoft_Stakeholder_Engagement_Manager__c);
        }
            
        Map<Id, Id> committeeCodeMap = new Map<Id, Id>();

        if (!committeeCodes.isEmpty()) {
            committeeCodeMap = getCommitteeCodeAndIdMap(committeeCodes);
        }  
            
        Map<String, Id> sectorCodeMap = new Map<String, Id>();    

        if (!sectorCodes.isEmpty()) {
            sectorCodeMap = getSectorCodeandIdMap(sectorCodes);
        }

        Map<String, Id> projectIDMap = new Map<String, Id>();

        if (!projectManagerEmails.isEmpty()) {
            projectIDMap = getUserEmailAndIdMap(projectManagerEmails);
        }

        Map<Id, Id> programManagerIdsMap = new Map<Id, Id>();    

        if (!programManagerIds.isEmpty()) {
            programManagerIdsMap = getUserIDs(programManagerIds);
        }

        Map<Id, Id> stakeholderEngManagerIDMap = new Map<Id, Id>();    

        if (!stakeholderEngManagerIDs.isEmpty()) {
            stakeholderEngManagerIDMap = getUserIDs(stakeholderEngManagerIDs);}
            
            for (Project__c p: projectList) {
                if ((!committeeCodeMap.isEmpty() || committeeCodeMap != null) && committeeCodeMap.containsKey(p.Mulesoft_National_Committee_Code__c)) {
                    Id id1 = committeeCodeMap.get(p.Mulesoft_National_Committee_Code__c);

                    p.National_Committee__c = committeeCodeMap.get(p.Mulesoft_National_Committee_Code__c);
                }

                if ((!sectorCodeMap.isEmpty() || sectorCodeMap != null) && sectorCodeMap.containsKey(p.Mulesoft_Sector_Code__c)) {
                    p.Sector__c = sectorCodeMap.get(p.Mulesoft_Sector_Code__c);
                }

                if ((!projectIDMap.isEmpty() || projectIDMap != null) && projectIDMap.containsKey(p.Mulesoft_Project_Manager_Email__c.toUpperCase())) {
                    p.Project_Manager__c = projectIDMap.get(p.Mulesoft_Project_Manager_Email__c.toUpperCase());
                }

                if ((!programManagerIdsMap.isEmpty() || programManagerIdsMap != null) && programManagerIdsMap.containsKey(p.Mulesoft_Program_Manager__c)) {
                    p.Program_Manager__c = programManagerIdsMap.get(p.Mulesoft_Program_Manager__c);
                }
                
                if ((!stakeholderEngManagerIDMap.isEmpty() || stakeholderEngManagerIDMap != null) && stakeholderEngManagerIDMap.containsKey(p.Mulesoft_Stakeholder_Engagement_Manager__c)) {
                    p.Stakeholder_Engagement_Manager__c = stakeholderEngManagerIDMap.get(p.Mulesoft_Stakeholder_Engagement_Manager__c);
                }
            }
        }
    }

    public static void doBeforeUpdate(List<Project__c> projectList) {
        if (UserInfo.getName() == 'Integration User') {

            Set<Id> committeeCodes = new Set<Id>();
            Set<String> sectorCodes = new Set<String>();
            Set<String> projectManagerEmails = new Set<String>();
            Set<String> programManagerIDs = new Set<String>();
            Set<String> stakeholderEngManagerIDs = new Set<String>();
            
            for (Project__c p: projectList) {
                if (String.isNotBlank(p.Mulesoft_National_Committee_Code__c)) {
                    committeeCodes.add(p.Mulesoft_National_Committee_Code__c);
                }

                sectorCodes.add(p.Mulesoft_Sector_Code__c);
                projectManagerEmails.add(p.Mulesoft_Project_Manager_Email__c);
                programManagerIds.add(p.Mulesoft_Program_Manager__c);
                stakeholderEngManagerIDs.add(p.Mulesoft_Stakeholder_Engagement_Manager__c);
            }
			
            Map<Id, Id> committeeCodeMap = new Map<Id, Id>();
            
            if (!committeeCodes.isEmpty()) {
                committeeCodeMap = getCommitteeCodeAndIdMap(committeeCodes);
            }

            Map<String, Id> sectorCodeMap = new Map<String, Id>();

            if (!sectorCodes.isEmpty()) {
                sectorCodeMap = getSectorCodeandIdMap(sectorCodes);
            }

            Map<String, Id> projectIDMap = new Map<String, Id>();

            if (!projectManagerEmails.isEmpty()) {
                projectIDMap = getUserEmailAndIdMap(projectManagerEmails);
            }

            Map<Id, Id> programManagerIdsMap = new Map<Id, Id>();

            if (!programManagerIds.isEmpty()) {
                programManagerIdsMap = getUserIDs(programManagerIds);
            }

            Map<Id, Id> stakeholderEngManagerIDMap = new Map<Id, Id>();  

            if (!stakeholderEngManagerIDs.isEmpty()) {
                stakeholderEngManagerIDMap = getUserIDs(stakeholderEngManagerIDs);
            }
            
            for (Project__c p: projectList) {
                if ((!committeeCodeMap.isEmpty() || committeeCodeMap != null) && committeeCodeMap.containsKey(p.Mulesoft_National_Committee_Code__c)) {
                    p.National_Committee__c = committeeCodeMap.get(p.Mulesoft_National_Committee_Code__c);
                }

                if ((!sectorCodeMap.isEmpty() || sectorCodeMap != null) && sectorCodeMap.containsKey(p.Mulesoft_Sector_Code__c)) {
                    p.Sector__c = sectorCodeMap.get(p.Mulesoft_Sector_Code__c);
                }

                if ((!projectIDMap.isEmpty() || projectIDMap != null) && projectIDMap.containsKey(p.Mulesoft_Project_Manager_Email__c.toUpperCase())) {
                   p.Project_Manager__c = projectIDMap.get(p.Mulesoft_Project_Manager_Email__c.toUpperCase());
                }

                if ((!programManagerIdsMap.isEmpty() || programManagerIdsMap != null) && programManagerIdsMap.containsKey(p.Mulesoft_Program_Manager__c)) {
                    p.Program_Manager__c = programManagerIdsMap.get(p.Mulesoft_Program_Manager__c);
                }
                
                if ((!stakeholderEngManagerIDMap.isEmpty() || stakeholderEngManagerIDMap != null) && stakeholderEngManagerIDMap.containsKey(p.Mulesoft_Stakeholder_Engagement_Manager__c)) {
                    p.Stakeholder_Engagement_Manager__c = stakeholderEngManagerIDMap.get(p.Mulesoft_Stakeholder_Engagement_Manager__c);
                }
            }
       }
    }

    public static Map<Id, Id> getCommitteeCodeAndIdMap(Set<Id> committeeCodes) {
        Map<Id, Id> committeeCodeMap = new Map<Id, Id>();
        
        for (Account x : [SELECT Id, Name FROM Account WHERE Id IN :committeeCodes]) {            
            committeeCodeMap.put(x.Id,x.Id);
        }

        return committeeCodeMap;
    }

    public static Map<Id, Id> getUserIDs(Set<String> userIds) {
        Map<Id, Id> userIdMap = new Map<Id, Id>();
        
        for (User x : [SELECT Id, Email FROM User WHERE Id IN :userIds]) {
            userIdMap.put(x.Id,x.Id);
        }
        
        return userIdMap;
    }

    public static Map<String, Id> getSectorCodeandIdMap(Set<String> sectorCodes) {
        Map<String, Id> sectorCodeMap = new Map<String, Id>();
        
        for (Sector__c x : [SELECT Id, Sector_Code__c,Name FROM Sector__c WHERE Name IN: sectorCodes]) {
            sectorCodeMap.put(x.Name, x.Id);
        }
        
        return sectorCodeMap;
    }

    public static Map<String, Id> getUserEmailAndIdMap(Set<String> userEmails) {
        Map<String, Id> userEmailMap = new Map<String, Id>();
        
        for (User x : [SELECT Id, Email FROM User WHERE Email IN: userEmails]) {
            userEmailMap.put(x.Email.toUpperCase(), x.Id);
        }

        return userEmailMap;
    }
}