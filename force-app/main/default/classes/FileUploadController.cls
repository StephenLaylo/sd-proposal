/**
    @author:            Stephen Laylo
    @company:           J4RVIS
    @description:       Controller class for fileUpload Lightning Web Component
*/
public with sharing class FileUploadController {

    /**
        @author:            Stephen Laylo
        @company:           J4RVIS
        @description:       Method to retrieve Related Files of a specific record
        @params:            recordId         -    ID of the record
    */
    @AuraEnabled(cacheable=true)
    public static List<file> getRelatedFiles(String recordId) {
        List<File> files = new List<File>();

        List<ContentDocumentLink> contentDocumentLinks = [
            SELECT ContentDocument.Id, 
                   ContentDocument.Title, 
                   ContentDocument.Description, 
                   ContentDocument.CreatedDate, 
                   ContentDocument.FileType, 
                   ContentDocument.ContentSize, 
                   ContentDocument.Owner.Name 
            FROM ContentDocumentLink 
            WHERE LinkedEntityId = :recordId
        ];

        for (ContentDocumentLink conLink : contentDocumentLinks) {
            File file = new File();

            file.title = conLink.ContentDocument.Title;
            file.description = conLink.ContentDocument.Description;
            file.id = conLink.ContentDocument.Id;
            file.createdDate = conLink.ContentDocument.CreatedDate;
            file.type = conLink.ContentDocument.FileType;
            file.owner = conLink.ContentDocument.Owner.Name;
            file.size = CommonUtils.getFileSizeToString((Long) conLink.ContentDocument.ContentSize);

            files.add(file);
        }

        return files;
    }

    public class File {
        @AuraEnabled
        public String title;

        @AuraEnabled
        public String description;

        @AuraEnabled
        public String type;

        @AuraEnabled
        public String owner;

        @AuraEnabled
        public String size;

        @AuraEnabled
        public Id id;

        @AuraEnabled
        public Datetime createdDate;
    }
}