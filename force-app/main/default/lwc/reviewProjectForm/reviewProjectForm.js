import { api, LightningElement, track } from 'lwc';

export default class ReviewProjectForm extends LightningElement {

    @api constants;
    @api questionsMap;
    @api project;
    @api isEditMode = false;

    handleProjectInputUpdate(event) {
        try {            
            let projectTemp = JSON.parse(JSON.stringify(this.project));                    
            projectTemp.projectRecord[event.target.name] = event.target.value;            
            
            this.project = Object.assign({}, projectTemp);           

            const evtCustomEvent = new CustomEvent('updateproject', 
                {
                    bubbles: true,
                    composed: true,
                    detail: {
                        project: this.project
                    }
                }
            );  

            this.dispatchEvent(evtCustomEvent);

        } catch (error) {
            console.log(error);
        }
    }

    get isAdoptionModifiedContent() {
        return (this.project.Project_Type__c == this.constants.PROJECT_TYPE_INTERNATIONAL_ADOPTION_MODIFIED);
    }

}