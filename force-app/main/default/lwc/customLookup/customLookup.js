/**
    @author:            Stephen Laylo & Jen Tan
    @company:           J4RVIS
    @description:       JS Controller for reusable Custom Lookup LWC
**/

import { LightningElement, api, track } from 'lwc';
import retrieveRecordsByIds from '@salesforce/apex/CustomLookupController.retrieveRecordsByIds';
import searchForRecords from '@salesforce/apex/CustomLookupController.searchForRecords';

let i = 0;

export default class CustomLookup extends LightningElement {

    @track globalSelectedItems = [];

    @api selectedItems;
    @api selectedItem;
    @api labelName;
    @api objectApiName;
    @api fieldApiNames;
    @api filterFieldApiName;
    @api iconName;
    @api noMatchFoundText;
    @api isMultiSelect;

    @track items = [];
    @track selectedItems = [];
    @track selectedItem;

    @track previousSelectedItems = []; 
    @track value = [];
    searchInput = '';
    isDialogDisplay = false;
    isDisplayMessage = false;

    connectedCallback() {
        if (this.selectedItems != null && this.selectedItems != '') {
            let currentSelectedValues = this.selectedItems.split(',');

            retrieveRecordsByIds({
                objectName: this.objectApiName,
                fieldAPINames: this.fieldApiNames,
                recordIds: currentSelectedValues

            }).then(result => {
                if (result.length > 0) {
                    result.map(resElement => {

                        this.globalSelectedItems.push({
                            value: resElement.recordId, 
                            label: resElement.recordName + ' • ' + resElement.title, 
                            name: resElement.recordName 
                        });
                    });
                }

            }).catch(error => {
                this.error = error;
                this.items = undefined;
            })
        }        
    }
    
    onchangeSearchInput(event) {
        this.searchInput = event.target.value;

        if (this.searchInput.trim().length > 0) {
            searchForRecords({
                objectName: this.objectApiName,
                fieldAPINames: this.fieldApiNames,
                filterFieldAPIName: this.filterFieldApiName,
                strInput: this.searchInput

            }).then(result => {

                const evtCustomEvent = new CustomEvent('searchresult', {   
                    detail: {
                        searchResult: result
                    }
                });
        
                this.dispatchEvent(evtCustomEvent);

                this.items = [];
                this.value = [];
                this.previousSelectedItems = [];

                if (result.length > 0) {
                    result.map(resElement => {
                        this.items = [
                            ...this.items,
                            {
                                value: resElement.recordId, 
                                label: resElement.recordName + ' • ' + resElement.title, 
                                name: resElement.recordName
                            }
                        ];
                        
                        this.globalSelectedItems.map(element => {
                            if (element.value == resElement.recordId) {
                                this.value.push(element.value);
                                this.previousSelectedItems.push(element);                      
                            }
                        });
                    });

                    this.isDialogDisplay = true;
                    this.isDisplayMessage = false;

                } else {
                    this.isDialogDisplay = false;
                    this.isDisplayMessage = true;                    
                }

            }).catch(error => {
                const evtCustomEvent = new CustomEvent('searchresult', {   
                    detail: {
                        searchResult: null
                    }
                });
        
                this.dispatchEvent(evtCustomEvent);

                this.error = error;
                this.items = undefined;
                this.isDialogDisplay = false;
            })

        } else {
            this.isDialogDisplay = false;
        }                
    }

    handleCheckboxChange(event) {
        let selectItemTemp = event.detail.value;
        this.selectedItems = [];   

        selectItemTemp.map(p => {            
            let arr = this.items.find(element => element.value == p);

            if (arr != undefined) {
                this.selectedItems.push(arr);
            }  
        });     
    }

    handleSelectSingleRecord(event) {
        this.selectedItem = this.items.find(item => (item.value === event.currentTarget.dataset.value));

        console.log('~~ this.selectedItem : ', this.selectedItem);

        this.initializeValues();

        const evtCustomEvent = new CustomEvent('selectsingle', { 
            detail: {
                selectedItem: this.selectedItem
            }
        });

        this.dispatchEvent(evtCustomEvent);
    }

    handleRemoveSingleRecord(event) {
        this.selectedItem = null;

        this.initializeValues();

        const evtCustomEvent = new CustomEvent('removesingle', {   
            detail: {
                selectedItem: this.selectedItem
            }
        });

        this.dispatchEvent(evtCustomEvent);
    }

    handleRemoveRecord(event) {
        const removeItem = event.target.dataset.item;
        
        this.globalSelectedItems = this.globalSelectedItems.filter(item => item.value != removeItem);
        const arrItems = this.globalSelectedItems;

        this.initializeValues();
        this.value = []; 

        const evtCustomEvent = new CustomEvent('remove', {   
            detail: {
                removeItem,
                arrItems
            }
        });

        this.dispatchEvent(evtCustomEvent);
    }

    handleDoneClick(event) {
        this.previousSelectedItems.map(p => {
            this.globalSelectedItems = this.globalSelectedItems.filter(item => item.value != p.value);
        });
        
        this.globalSelectedItems.push(...this.selectedItems);        
        const arrItems = this.globalSelectedItems;
        
        this.previousSelectedItems = this.selectedItems;
        this.initializeValues();
        
        const evtCustomEvent = new CustomEvent('retrieve', { 
            detail: {
                arrItems
            }
        });

        this.dispatchEvent(evtCustomEvent);
    }

    handleCancelClick(event) {
        this.initializeValues();
    }
    
    initializeValues() {
        this.searchInput = '';        
        this.isDialogDisplay = false;
    }

}