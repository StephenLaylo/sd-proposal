/**
    @author:            Stephen Laylo & Jen Tan
    @company:           J4RVIS
    @description:       JS Controller for FileList LWC that handles viewing of list of Files related to a record
**/

import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { deleteRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { updateRecord } from 'lightning/uiRecordApi';
import { reduceErrors } from 'c/ldsUtils';
import ID_FIELD from '@salesforce/schema/ContentDocument.Id';
import DESCRIPTION_FIELD from '@salesforce/schema/ContentDocument.Description';

const actions = [
    { 
        label: 'View File Details', 
        name: 'view_file_details' 
    }, 
    { 
        label: 'Edit File Details', 
        name: 'edit_file_details' 
    }, 
    { 
        label: 'Delete', 
        name: 'delete' 
    }
];

const fileColumns = [
    { 
        label: 'Title', 
        fieldName: 'title', 
        type: 'text', 
        wrapText: true
    }, 
    { 
        label: 'Description', 
        fieldName: 'description', 
        type: 'text', 
        sortable: true, 
        wrapText: true
    }, 
    { 
        label: 'Owner', 
        fieldName: 'owner', 
        type: 'text', 
        wrapText: true
    }, 
    { 
        label: 'Size Date', 
        fieldName: 'size', 
        type: 'text', 
        wrapText: true
    }, 
    { 
        label: 'Type', 
        fieldName: 'type', 
        type: 'text', 
        wrapText: true
    }, 
    { 
        label: 'Action', 
        type: 'action', 
        typeAttributes: { 
            rowActions: actions
        }
    }
];

export default class FileList extends NavigationMixin(LightningElement) {

    @api files;
    @api viewOnly;

    @track originalMessage;
    @track selectedRow;
    @track isDialogVisible = false;
    
    @track showFileDetails = false;
    @track editFileDetails = false;

    fileColumns = fileColumns;

    handleRowAction(event) {
        const actionName = event.detail.action.name;
        const row = event.detail.row;

        switch (actionName) {
            case 'delete':
                this.originalMessage = row.id;
                this.isDialogVisible = true;
                break;

            case 'view_file_details':
                this.selectedRow = row;
                this.showFileDetails = true;
                this.editFileDetails = false;
                break;
                
            case 'edit_file_details':
                this.selectedRow = row;
                this.showFileDetails = true;
                this.editFileDetails = true;
                break;

            default:
        }
    }

    handleDelete(event) {
        if (event.target) {
            if (event.target.name === 'confirmModal') {
                if (event.detail !== 1) {
                    if (event.detail.status === 'confirm') {
                        //delete content document
                        let contentDocumentId = event.detail.originalMessage;

                        deleteRecord(contentDocumentId).then(() => {
                            this.dispatchEvent(
                                new ShowToastEvent({
                                    title: 'Success',
                                    message: 'File deleted',
                                    variant: 'success'
                                })
                            );

                            this.dispatchEvent(new CustomEvent('filedelete', {}));

                        }).catch(error => {
                            this.dispatchEvent(
                                new ShowToastEvent({
                                    title: 'Error deleting file',
                                    message: error.body.message,
                                    variant: 'error'
                                })
                            );
                        });
                    }
                }

                //hides the component
                this.isDialogVisible = false;
            }
        }
    }

    handleConfirmDescriptionUpdate(event) {        
        const descriptionValue = this.template.querySelector('lightning-textarea[data-id="'+ event.currentTarget.dataset.id + '"][data-name="fileDescription"]').value;

        if (descriptionValue) {
            const fields = {};

            fields[ID_FIELD.fieldApiName] = event.currentTarget.dataset.id;
            fields[DESCRIPTION_FIELD.fieldApiName] = descriptionValue;

            const recordInput = { fields };

            updateRecord(recordInput).then(() => {
                const evtCustomEvent = new CustomEvent('refreshfiles');  
                this.dispatchEvent(evtCustomEvent);

                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success!',
                        message: 'File description updated.',
                        variant: 'success'
                    })
                );
            }).catch(error => {
                error = reduceErrors(error);
                this.error = error;

                console.log(error);
            });

            this.toggleEditButton(true);        
            this.toggleEditDescription(event.currentTarget.dataset.id, false);
        }
    }
    
    handleCancelDescriptionUpdate(event) {
        const refreshFilesEvt = new CustomEvent('refreshfiles');

        this.dispatchEvent(refreshFilesEvt);
        this.toggleEditButton(true);
        this.toggleEditDescription(event.currentTarget.dataset.id, false);
    }

    onEditDescriptionBtnClick(event) {
        this.toggleEditButton(false);
        this.toggleEditDescription(event.currentTarget.dataset.id, true);
    }

    toggleEditDescription(dataId, showEdit) {
        const descriptionTextAreaElem = this.template.querySelector('div[data-id="'+ dataId + '"][data-name="editDescription"]');
        const descriptionDisplayElement = this.template.querySelector('div[data-id="'+ dataId + '"][data-name="descriptionView"]');

        if (descriptionTextAreaElem && descriptionDisplayElement) {
            if (showEdit) {
                descriptionTextAreaElem.classList.remove('hidden');
                descriptionDisplayElement.classList.add('hidden');
                this.template.querySelector('lightning-textarea[data-id="'+ dataId + '"][data-name="fileDescription"]').focus();
    
            } else {
                descriptionTextAreaElem.classList.add('hidden');
                descriptionDisplayElement.classList.remove('hidden');
            }
        }
    }

    toggleEditButton(show) {
        const divEditButtons = this.template.querySelectorAll('div[data-name="descriptionEditBtn"]');

        if (divEditButtons) {
            divEditButtons.forEach(function (divEditButton) {
                if (show) {
                    divEditButton.classList.remove('hidden');
    
                } else {
                    divEditButton.classList.add('hidden');
                }                        
            });
        } 
    }

    handleCloseFileDetails(event) {
        this.showFileDetails = false;
    }

    get hasAttachments() {
        return (this.files != null && this.files.length > 0);
    }

}