import { LightningElement, api, track } from 'lwc';

export default class ReviewProposalForm extends LightningElement {

    @api constants;
    @api questionsMap;
    @api isEditMode = false;
    @api proposalRecord;
    @api projects;

    handleEdit(event) {
        this.isEditMode = !this.isEditMode;
    }

    handleProposalInputUpdate(event) {
        try {           
            let proposalRecordTemp = JSON.parse(JSON.stringify(this.proposalRecord));
            proposalRecordTemp[event.target.name] = event.target.value;            
            
            this.proposalRecord = Object.assign({}, proposalRecordTemp);           

            const evtCustomEvent = new CustomEvent('updateproposal', 
                {                    
                    detail: {
                        proposalRecord: this.proposalRecord
                    }
                }
            );  

            this.dispatchEvent(evtCustomEvent);

        } catch (error) {
            console.log(error);
        }
    }

}