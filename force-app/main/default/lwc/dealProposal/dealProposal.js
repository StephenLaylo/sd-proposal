/**
    @author:            Stephen Laylo & Jen Tan
    @company:           J4RVIS
    @description:       JS Controller for ProposalForm LWC that handles all transactions & validations of the flow screen
**/

import { LightningElement, track, api, wire } from 'lwc';
import { reduceErrors } from 'c/ldsUtils';
import getPicklistValuesMap from '@salesforce/apex/CommonUtils.getPicklistValues';
import upsertProposalAndProjects from '@salesforce/apex/ProposalFormController.upsertProposalAndProjects';
import PROPOSAL_OBJECT from '@salesforce/schema/Proposal__c';

export default class ProposalForm extends LightningElement {
    
    @track isRendering = true;
    @track error;

    @api constants;

    /* VARIABLES FOR CONFIGURATIONS FOR LABELS */
    @api proposalQuestion; // = 'What are you wanting to propose?';
    @api publicationTypeQuestion; // = 'What type of publication are you proposing?';
    @api relatedToAnyExistingPublicationsQuestion; // = 'Does your proposal relate to any existing publications?';
    @api multipleRelatedPublicationsSearchQuestion; //= 'Please select the publications that are related to your proposal';
    @api proposedPublicationTitleQuestion; // = 'What will the title of your proposed publication be?';
    @api problemDesignedToSolveQuestion; // = 'What problem is this designed to solve?';
    @api proposedPublicationScopeQuestion; // = 'What is the scope of the new publication? (be as detailed & comprehensive as possible)';
    @api revisePublicationsSearchQuestion; // = 'Search for publication you want to revise/amend';
    @api revisePublicationDesignationTitleQuestion; // = 'Enter Designation & Title of publication you want to revise/amend';
    @api revisedPublicationDesignationQuestion; // = 'Designation';
    @api revisedPublicationTitleQuestion; // = 'Title';
    @api currentEditionIssuesQuestion; // = 'What are the issues with the current edition?';
    @api evidenceExistsProblemQuestion; // = 'What evidence exists to demonstrate there is a problem?';
    @api proposedSolutionQuestion; // = 'What is your proposed solution (be as detailed & comprehensive as possible)?';
    @api multipleInternationalPublicationsSearchQuestion; // = 'Search for international publication you want to adopt';
    @api adoptInternationalStandardDesignationTitleQuestion; // = 'Enter Designation & Title of International Standard you want to adopt';
    @api adoptedPublicationDesignationQuestion; // = 'Designation';
    @api adoptedPublicationTitleQuestion; // = 'Title';
    @api adoptionIdenticalOrModifyContentQuestion; // = 'Is this adoption identical, or do you need to modify the content?';
    @api whyPublicationBeAdoptedInAustraliaQuestion; // = 'Why should this publication be adopted in Australia?';
    @api whyCannotAdoptedIdenticallyCurrentInternationalStandardQuestion; // = 'Why can’t the current international standard be adopted identically (refer to TBT agreement information)';
    @api proposedVariationsQuestion; // = 'What are your proposed variations (be as detailed as possible)';
    @api addMoreProjectsQuestion; // = 'Do you want to add more projects to your proposal?';
    @api benefitsToAustralianCommunityPublicHealthAndSafetyQuestion; // = 'How will this work bring Net Benefit to the Australian Community - Public and health safety?';
    @api benefitsToAustralianCommunitySocialAndCommunityImpactQuestion; // = 'How will this work bring Net Benefit to the Australian Community - Social and community impact?';
    @api benefitsToAustralianCommunityEnvironmentalImpactQuestion; // = 'How will this work bring Net Benefit to the Australian Community - Environmental impact?';
    @api benefitsToAustralianCommunityCompetitionQuestion; // = 'How will this work bring Net Benefit to the Australian Community - Competition?';
    @api benefitsToAustralianCommunityEconomicImpactQuestion; // = 'How will this work bring Net Benefit to the Australian Community - Economic impact?';
    @api consultedPeopleQuestion; // = 'Who has been consulted and what are their views?';
    @api declarationText; // = 'Review and agree to Declaration (including Privacy Notice)';
    @api doNotAgreeText; // = 'You are required to agree with the declaration terms before you can submit this proposal. If you do not agree to the terms please contact a SEM to discuss: www.standards.org.au/engagementevents/ sectors';
    @api thankYouText; // = 'Thank you for submitting your proposal';
    @api noMatchFoundText; // = 'No Match Found. Please search again, or enter the designation and title below.';

    /* VARIABLES FOR FIELDS */
    @api projects = [];
    @track proposalType;
    @track proposalId;
    @track currentProjectId;
    @track publicationType;
    @track isRelatedToAnyExistingPublications;
    @track selectedRelatedPublications = [];
    @track proposedPublicationTitle;
    @track problemDesignedToSolve;
    @track proposedPublicationScope;
    @track selectedPublicationsToBeRevised = [];
    @track selectedPublicationToBeRevised;
    @track revisedPublicationDesignation;
    @track revisedPublicationTitle;
    @track currentEditionIssues;
    @track evidenceExistsProblem;
    @track proposedSolution;
    @track selectedInternationalPublications = [];
    @track selectedInternationalPublication;
    @track adoptedPublicationDesignation;
    @track adoptedPublicationTitle;
    @track adoptionIdenticalOrModifyContent;
    @track whyPublicationBeAdoptedInAustraliaReason;
    @track whyCannotAdoptedIdenticallyCurrentInternationalStandardReason;
    @track proposedVariations;
    @track willAddMoreProjects;
    @track disclaimerCheck = false;
    @track showDataRequestForAmend = false;
    @track showDataRequestForAdopt = false;

    /* SCREENS */
    @track proposalScreen = true;
    @track createNewPublicationScreen1 = false;
    @track createNewPublicationScreen2 = false;
    @track amendExistingPublicationsScreen1 = false;
    @track amendExistingPublicationsScreen2 = false;
    @track adoptInternationalPublicationsScreen1 = false;
    @track adoptInternationalPublicationsScreen2 = false;
    @track addMoreProposalScreen = false;
    @track netBenefitScreen1 = false;
    @track netBenefitScreen2 = false;
    @api reviewAllProposedProjectsScreen = false;
    @track declarationAndPrivacyNoticeScreen = false;
    @track thankYouScreen = false;

    @track publicationTypeChoices;

    @api proposalRecord;
    @track isReviewEditMode = false;

    relatedToAnyExistingPublicationsChoices = {
        data: {
            values: [
                {
                    label: 'Yes',
                    value: 'Yes'
                },
                {
                    label: 'No',
                    value: 'No'
                }
            ]
        }
    };

    addMoreProjectsChoices = {
        data: {
            values: [
                {
                    label: 'Yes',
                    value: 'Yes'
                },
                {
                    label: 'No',
                    value: 'No'
                }
            ]
        }
    };

    disclaimerCheckChoices = {
        data: {
            values: [
                {
                    label: 'Agree',
                    value: 'Agree'
                },
                {
                    label: 'Disagree',
                    value: 'Disagree'
                }
            ]
        }
    };

    questionsMap = {};

    @track selectedItemsToDisplay = '';

    selectItemEventHandler(event) {
        let args;

        if (event.detail.arrItems) {
            args = JSON.parse(JSON.stringify(event.detail.arrItems));
            console.log('~~ arrItems args : ', args);

            this.displayItem(args);  

        } else if (event.detail.selectedItem) {
            args = JSON.parse(JSON.stringify(event.detail.selectedItem));

            console.log('~~ args : ', args);

            this.selectedPublicationToBeRevised = args;
            this.selectedInternationalPublication = args;
        }
    }

    deleteItemEventHandler(event) {
        let args;

        if (event.detail.arrItems) {
            args = JSON.parse(JSON.stringify(event.detail.arrItems));
            console.log('~~ delete args : ', args);
            this.displayItem(args);  

        } else if (event.detail.selectedItem) {
            args = JSON.parse(JSON.stringify(event.detail.selectedItem));

            this.selectedPublicationToBeRevised = null;
            this.selectedInternationalPublication = null;
        }
    }

    selectInternationalPublicationEventHandler(event) {
        let args;

        if (event.detail.selectedItem) {
            args = JSON.parse(JSON.stringify(event.detail.selectedItem));

            this.selectedInternationalPublication = args;
        }
    }

    deleteInternationalPublicationEventHandler(event) {
        this.selectedInternationalPublication = null;
    }

    selectExistingPublicationEventHandler(event) {
        let args;

        if (event.detail.selectedItem) {
            args = JSON.parse(JSON.stringify(event.detail.selectedItem));

            this.selectedPublicationToBeRevised = args;
        }
    }

    deleteExistingPublicationEventHandler(event) {
        this.selectedPublicationToBeRevised = null;
    }

    displayItem(args) {
        let selectedPublications = [];

        args.map(element => {
            selectedPublications.push(element.value);
        });

        this.selectedItemsToDisplay = selectedPublications.join(', ');

        // this.selectedRelatedPublications = selectedPublications;
        this.selectedRelatedPublications = args;
        this.selectedPublicationsToBeRevised = selectedPublications;
        this.selectedInternationalPublications = selectedPublications;
    }

    connectedCallback() {
        this.proposalTypeChoices = (!this.constants) ? null : { 
            data: { 
                values: [
                    {
                        value : this.constants.PROJECT_PROPOSAL_TYPE_CREATE_NEW, 
                        label : this.constants.PROJECT_PROPOSAL_TYPE_CREATE_NEW
                    }, 
                    {
                        value : this.constants.PROJECT_PROPOSAL_TYPE_REVISE_EXISTING, 
                        label : this.constants.PROJECT_PROPOSAL_TYPE_REVISE_EXISTING
                    }, 
                    {
                        value : this.constants.PROJECT_PROPOSAL_TYPE_ADOPT_INTERNATIONAL, 
                        label : this.constants.PROJECT_PROPOSAL_TYPE_ADOPT_INTERNATIONAL
                    }
                ] 
            }
        };
    
        this.adoptionIdenticalOrModifyContentChoices = (!this.constants) ? null : {
            data: {
                values: [
                    {
                        label: this.constants.PROJECT_TYPE_CHOICES_IDENTICAL,
                        value: this.constants.PROJECT_TYPE_CHOICES_IDENTICAL
                    },
                    {
                        label: this.constants.PROJECT_TYPE_CHOICES_MODIFIED,
                        value: this.constants.PROJECT_TYPE_CHOICES_MODIFIED
                    }
                ]
            }
        };

        getPicklistValuesMap({ 
            objectName: 'Project__c',
            fieldName : 'Product_Type__c' 

        }).then(result => {
            result = JSON.parse(JSON.stringify(result));

            let data = [];

            for (let x = 0; x < Object.keys(result).length; x ++) {
                let val = Object.keys(result)[x];

                data.push({
                    value : val, 
                    label : result[val]
                });
            }

            data = { values: data }; 

            this.publicationTypeChoices = { 
                data: data
            };

            this.error = null;
            this.isRendering = false;

        }).catch(error => {
            error = reduceErrors(error);

            this.publicationTypeChoices = null;
            this.error = 'Unknown error ' + error;
            this.isRendering = false;
        });

        if (this.reviewAllProposedProjectsScreen) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            this.isReviewEditMode = true;
            this.proposalId = this.proposalRecord.Id;

            if (this.projects && this.projects.length > 0) {
                this.currentProjectId = this.projects[this.projects.length - 1].projectRecord.Id;
            }

        } else {
            this.initializeValues();
        }
        
        this.questionsMap = {
            proposalQuestion : this.proposalQuestion,
            publicationTypeQuestion : this.publicationTypeQuestion,
            relatedToAnyExistingPublicationsQuestion : this.relatedToAnyExistingPublicationsQuestion,
            multipleRelatedPublicationsSearchQuestion : this.multipleRelatedPublicationsSearchQuestion,
            proposedPublicationTitleQuestion : this.proposedPublicationTitleQuestion,
            problemDesignedToSolveQuestion : this.problemDesignedToSolveQuestion,
            proposedPublicationScopeQuestion : this.proposedPublicationScopeQuestion,
            revisePublicationsSearchQuestion : this.revisePublicationsSearchQuestion,
            revisePublicationDesignationTitleQuestion : this.revisePublicationDesignationTitleQuestion,
            revisedPublicationDesignationQuestion : this.revisedPublicationDesignationQuestion,
            revisedPublicationTitleQuestion : this.revisedPublicationTitleQuestion,
            currentEditionIssuesQuestion : this.currentEditionIssuesQuestion,
            evidenceExistsProblemQuestion : this.evidenceExistsProblemQuestion,
            proposedSolutionQuestion : this.proposedSolutionQuestion,
            multipleInternationalPublicationsSearchQuestion : this.multipleInternationalPublicationsSearchQuestion,
            adoptInternationalStandardDesignationTitleQuestion : this.adoptInternationalStandardDesignationTitleQuestion,
            adoptedPublicationDesignationQuestion : this.adoptedPublicationDesignationQuestion,
            adoptedPublicationTitleQuestion : this.adoptedPublicationTitleQuestion,
            adoptionIdenticalOrModifyContentQuestion : this.adoptionIdenticalOrModifyContentQuestion,
            whyPublicationBeAdoptedInAustraliaQuestion : this.whyPublicationBeAdoptedInAustraliaQuestion,
            whyCannotAdoptedIdenticallyCurrentInternationalStandardQuestion : this.whyCannotAdoptedIdenticallyCurrentInternationalStandardQuestion,
            proposedVariationsQuestion : this.proposedVariationsQuestion,
            addMoreProjectsQuestion : this.addMoreProjectsQuestion,
            benefitsToAustralianCommunityPublicHealthAndSafetyQuestion : this.benefitsToAustralianCommunityPublicHealthAndSafetyQuestion,
            benefitsToAustralianCommunitySocialAndCommunityImpactQuestion : this.benefitsToAustralianCommunitySocialAndCommunityImpactQuestion,
            benefitsToAustralianCommunityEnvironmentalImpactQuestion : this.benefitsToAustralianCommunityEnvironmentalImpactQuestion,
            benefitsToAustralianCommunityCompetitionQuestion : this.benefitsToAustralianCommunityCompetitionQuestion,
            benefitsToAustralianCommunityEconomicImpactQuestion : this.benefitsToAustralianCommunityEconomicImpactQuestion,
            consultedPeopleQuestion : this.consultedPeopleQuestion,
            declarationText : this.declarationText,
            doNotAgreeText : this.doNotAgreeText,
            thankYouText : this.thankYouText,
        }
    }

    initializeValues() {
        this.isRendering = true;
        this.proposalType = null;
        this.publicationType = null;
        this.isRelatedToAnyExistingPublications = null;
        this.selectedRelatedPublications = [];
        this.proposedPublicationTitle = null;
        this.problemDesignedToSolve = null;
        this.proposedPublicationScope = null;
        this.selectedPublicationsToBeRevised = [];
        this.selectedPublicationToBeRevised = null;
        this.revisedPublicationDesignation = null;
        this.revisedPublicationTitle = null;
        this.currentEditionIssues = null;
        this.evidenceExistsProblem = null;
        this.proposedSolution = null;
        this.selectedInternationalPublications = [];
        this.selectedInternationalPublication = null;
        this.adoptedPublicationDesignation = null;
        this.adoptedPublicationTitle = null;
        this.adoptionIdenticalOrModifyContent = null;
        this.whyPublicationBeAdoptedInAustraliaReason = null;
        this.whyCannotAdoptedIdenticallyCurrentInternationalStandardReason = null;
        this.proposedVariations = null;
        this.willAddMoreProjects = null;
        this.disclaimerCheck = false;
        this.selectedItemsToDisplay = [];
        this.currentProjectId = null;        

        this.proposalRecord = {
            apiName: PROPOSAL_OBJECT.objectApiName
        };
    }

    @api
    refreshComponent() {
        this.initializeValues();
        this.projects = [];
        this.proposalScreen = true;
        this.createNewPublicationScreen1 = false;
        this.createNewPublicationScreen2 = false;
        this.amendExistingPublicationsScreen1 = false;
        this.amendExistingPublicationsScreen2 = false;
        this.adoptInternationalPublicationsScreen1 = false;
        this.adoptInternationalPublicationsScreen2 = false;
        this.addMoreProposalScreen = false;
        this.netBenefitScreen1 = false;
        this.netBenefitScreen2 = false;
        this.reviewAllProposedProjectsScreen = false;
        this.declarationAndPrivacyNoticeScreen = false;
        this.thankYouScreen = false;
        this.isRendering = false;
        this.isReviewEditMode = false;

    }

    handleProposalTypeChange(event) {
        this.proposalType = event.detail.value;
    }

    handlePublicationTypeChange(event) {
        this.publicationType = event.detail.value;
    }

    handleRelatedToAnyExistingPublicationsChange(event) {
        this.isRelatedToAnyExistingPublications = event.detail.value;
    }

    handleAdoptionIdenticalOrModifyContentOnChange(event) {
        this.adoptionIdenticalOrModifyContent = event.detail.value;
    }

    handleAddMoreProjectsChange(event) {
        this.willAddMoreProjects = event.detail.value;
    }

    revisedPublicationDesignationOnBlur(event) {
        this.revisedPublicationDesignation = event.target.value;
    }

    revisedPublicationTitleOnBlur(event) {
        this.revisedPublicationTitle = event.target.value;
    }

    adoptedPublicationDesignationOnBlur(event) {
        this.adoptedPublicationDesignation = event.target.value;
    }

    adoptedPublicationTitleOnBlur(event) {
        this.adoptedPublicationTitle = event.target.value;
    }

    proposedPublicationTitleOnBlur(event) {
        this.proposedPublicationTitle = event.target.value;
    }

    problemDesignedToSolveOnBlur(event) {
        this.problemDesignedToSolve = event.target.value;
    }

    proposedPublicationScopeOnBlur(event) {
        this.proposedPublicationScope = event.target.value;
    }

    currentEditionIssuesOnBlur(event) {
        this.currentEditionIssues = event.target.value;
    }

    evidenceExistsProblemOnBlur(event) {
        this.evidenceExistsProblem = event.target.value;
    }

    proposedSolutionOnBlur(event) {
        this.proposedSolution = event.target.value;
    }

    whyPublicationBeAdoptedInAustraliaReasonOnBlur(event) {
        this.whyPublicationBeAdoptedInAustraliaReason = event.target.value;
    }

    whyCannotAdoptedIdenticallyCurrentInternationalStandardReasonOnBlur(event) {
        this.whyCannotAdoptedIdenticallyCurrentInternationalStandardReason = event.target.value;
    }

    proposedVariationsOnBlur(event) {
        this.proposedVariations = event.target.value;
    }

    revisedPublicationDesignationOnChange(event) {
        this.revisedPublicationDesignation = event.target.value;
    }

    revisedPublicationTitleOnChange(event) {
        this.revisedPublicationTitle = event.target.value;
    }

    adoptedPublicationDesignationOnChange(event) {
        this.adoptedPublicationDesignation = event.target.value;
    }

    adoptedPublicationTitleOnChange(event) {
        this.adoptedPublicationTitle = event.target.value;
    }

    proposedPublicationTitleOnChange(event) {
        this.proposedPublicationTitle = event.target.value;
    }

    problemDesignedToSolveOnChange(event) {
        this.problemDesignedToSolve = event.target.value;
    }

    proposedPublicationScopeOnChange(event) {
        this.proposedPublicationScope = event.target.value;
    }

    currentEditionIssuesOnChange(event) {
        this.currentEditionIssues = event.target.value;
    }

    evidenceExistsProblemOnChange(event) {
        this.evidenceExistsProblem = event.target.value;
    }

    proposedSolutionOnChange(event) {
        this.proposedSolution = event.target.value;
    }

    whyPublicationBeAdoptedInAustraliaReasonOnChange(event) {
        this.whyPublicationBeAdoptedInAustraliaReason = event.target.value;
    }

    whyCannotAdoptedIdenticallyCurrentInternationalStandardReasonOnChange(event) {
        this.whyCannotAdoptedIdenticallyCurrentInternationalStandardReason = event.target.value;
    }

    proposedVariationsOnChange(event) {
        this.proposedVariations = event.target.value;
    }

    handleProposalInput(event) {
        this.proposalRecord[event.target.name] = event.target.value;
    }

    handleDisclaimerCheck(event) {
        this.disclaimerCheck = event.target.value;
    }

    showDataRequestForAmendHandler(event) {
        let searchResult = event.detail.searchResult;

        if (searchResult != null && searchResult.length > 0) {
            this.showDataRequestForAmend = false;
            
        } else {
            this.showDataRequestForAmend = true;
        }

        this.revisedPublicationDesignation = null;
        this.revisedPublicationTitle = null;
        this.selectedPublicationToBeRevised = null;
    }

    showDataRequestForAdoptHandler(event) {
        let searchResult = event.detail.searchResult;

        if (searchResult != null && searchResult.length > 0) {
            this.showDataRequestForAdopt = false;

        } else {
            this.showDataRequestForAdopt = true;
        }

        this.adoptedPublicationDesignation = null;
        this.adoptedPublicationTitle = null;
        this.selectedInternationalPublication = null;
    }

    handleBackButton(event) {        
        if (this.createNewPublicationScreen1 === true) {
            this.proposalScreen = true;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;

        } else if (this.createNewPublicationScreen2 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = true;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;

        } else if (this.amendExistingPublicationsScreen1 === true) {
            this.proposalScreen = true;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.amendExistingPublicationsScreen2 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = true;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.adoptInternationalPublicationsScreen1 === true) {
            this.proposalScreen = true;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.adoptInternationalPublicationsScreen2 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = true;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.addMoreProposalScreen === true) {
            if (this.proposalType == this.constants.PROJECT_PROPOSAL_TYPE_CREATE_NEW) {
                this.proposalScreen = false;
                this.createNewPublicationScreen1 = false;
                this.createNewPublicationScreen2 = true;
                this.amendExistingPublicationsScreen1 = false;
                this.amendExistingPublicationsScreen2 = false;
                this.adoptInternationalPublicationsScreen1 = false;
                this.adoptInternationalPublicationsScreen2 = false;
                this.adoptInternationalPublicationsScreen3 = false;
                this.adoptInternationalPublicationsScreen4 = false;
                this.adoptInternationalPublicationsScreen5 = false;
                this.addMoreProposalScreen = false;
                this.netBenefitScreen1 = false;
                this.netBenefitScreen2 = false;
                this.reviewAllProposedProjectsScreen = false;
                this.declarationAndPrivacyNoticeScreen = false;
                this.thankYouScreen = false;

            } else if (this.proposalType == this.constants.PROJECT_PROPOSAL_TYPE_REVISE_EXISTING) {
                this.proposalScreen = false;
                this.createNewPublicationScreen1 = false;
                this.createNewPublicationScreen2 = false;
                this.amendExistingPublicationsScreen1 = false;
                this.amendExistingPublicationsScreen2 = true;
                this.adoptInternationalPublicationsScreen1 = false;
                this.adoptInternationalPublicationsScreen2 = false;
                this.adoptInternationalPublicationsScreen3 = false;
                this.adoptInternationalPublicationsScreen4 = false;
                this.adoptInternationalPublicationsScreen5 = false;
                this.addMoreProposalScreen = false;
                this.netBenefitScreen1 = false;
                this.netBenefitScreen2 = false;
                this.reviewAllProposedProjectsScreen = false;
                this.declarationAndPrivacyNoticeScreen = false;
                this.thankYouScreen = false;

            } else if (this.proposalType == this.constants.PROJECT_PROPOSAL_TYPE_ADOPT_INTERNATIONAL) {
                this.proposalScreen = false;
                this.createNewPublicationScreen1 = false;
                this.createNewPublicationScreen2 = false;
                this.amendExistingPublicationsScreen1 = false;
                this.amendExistingPublicationsScreen2 = false;
                this.adoptInternationalPublicationsScreen1 = false;
                this.adoptInternationalPublicationsScreen2 = true;
                this.addMoreProposalScreen = false;
                this.netBenefitScreen1 = false;
                this.netBenefitScreen2 = false;
                this.reviewAllProposedProjectsScreen = false;
                this.declarationAndPrivacyNoticeScreen = false;
                this.thankYouScreen = false;
            }

        } else if (this.netBenefitScreen1 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = true;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;

        } else if (this.netBenefitScreen2 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = true;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.reviewAllProposedProjectsScreen === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = true;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.declarationAndPrivacyNoticeScreen === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = true;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.thankYouScreen === true) {
            this.proposalScreen = true;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
        }
    }

    projectIndex = 1;    

    upsertProjectToMap() {
        let projectRecord = {
            'sObjectType': 'Project__c'
        };

        projectRecord.Id = this.currentProjectId;
        let selectedPublications = [];

        projectRecord.Project_Status__c = this.constants.PROJECT_STATUS_PROPOSAL;

        if (this.proposalType == this.constants.PROJECT_PROPOSAL_TYPE_CREATE_NEW) {

            /* CREATE NEW PUBLICATION */
            projectRecord.Project_Type__c = this.constants.PROJECT_TYPE_NEW;
            projectRecord.Product_Type__c = this.publicationType;
            projectRecord.Title__c = this.proposedPublicationTitle;
            projectRecord.What_problem_is_this_proposal_designed_t__c = this.problemDesignedToSolve;
            projectRecord.What_is_the_summarised_scope__c = this.proposedPublicationScope;
            selectedPublications = this.selectedRelatedPublications;

        } else if (this.proposalType == this.constants.PROJECT_PROPOSAL_TYPE_REVISE_EXISTING) {

            /* AMEND OR REVISE PUBLICATIONS */
            projectRecord.Project_Type__c = this.constants.PROJECT_TYPE_REVISED_TEXT_AMENDMENT;
            projectRecord.Issues_with_Current_edition__c = this.currentEditionIssues;
            projectRecord.What_evidence_exist__c = this.evidenceExistsProblem;
            projectRecord.What_is_your_Proposed_Solution__c = this.proposedSolution;

            if (this.selectedPublicationToBeRevised) {
                projectRecord.Existing_Publication_to_revise_amend__c = this.selectedPublicationToBeRevised.value;
                projectRecord.Title__c = this.selectedPublicationToBeRevised.name + ' - Amend';

            } else {
                projectRecord.Designation__c = this.revisedPublicationDesignation;
                projectRecord.Title__c = this.revisedPublicationTitle;
            }

        } else if (this.proposalType == this.constants.PROJECT_PROPOSAL_TYPE_ADOPT_INTERNATIONAL) {

            /* ADOPT INTERNATIONAL PUBLICATIONS */
            if (this.adoptionIdenticalOrModifyContent == this.constants.PROJECT_TYPE_CHOICES_IDENTICAL) {
                projectRecord.Project_Type__c = this.constants.PROJECT_TYPE_INTERNATIONAL_ADOPTION_IDENTICAL;

            } else if (this.adoptionIdenticalOrModifyContent == this.constants.PROJECT_TYPE_CHOICES_MODIFIED) {
                projectRecord.Project_Type__c = this.constants.PROJECT_TYPE_INTERNATIONAL_ADOPTION_MODIFIED;
            }

            projectRecord.Why_does_standard_need_to_be_adopted__c = this.whyPublicationBeAdoptedInAustraliaReason;
            projectRecord.Reasons_for_non_identical_adoption__c = this.whyCannotAdoptedIdenticallyCurrentInternationalStandardReason;
            projectRecord.What_are_your_Proposed_Variations__c = this.proposedVariations;

            if (this.selectedInternationalPublication) {
                projectRecord.International_Publication_to_adopt__c = this.selectedInternationalPublication.value;
                projectRecord.Title__c = this.selectedInternationalPublication.name + ' - IDT';

            } else {
                projectRecord.Designation__c = this.adoptedPublicationDesignation;
                projectRecord.Title__c = this.adoptedPublicationTitle;
            }
        }

        let projectRecordExisting = false;
        let self = this;
        //updates the existing project

        let tempProjects = Object.assign([], this.projects);

        tempProjects.forEach(function(obj, index) {
            if (self.projectIndex === obj.index) {
                this[index].projectRecord = projectRecord;
                this[index].selectedPublications = selectedPublications;
                
                projectRecordExisting = true;
            }

        }, tempProjects);
    
        this.projects = Object.assign([], tempProjects);

        if (!projectRecordExisting) {
            this.projects.push({
                projectRecord: projectRecord,
                // selectedPublications: (selectedPublications.length > 0 ? selectedPublications : null),
                selectedPublications: selectedPublications,
                isProposalCreateNewPublication: (this.proposalType == this.constants.PROJECT_PROPOSAL_TYPE_CREATE_NEW), 
                isProposalAmendingExistingPublications: (this.proposalType == this.constants.PROJECT_PROPOSAL_TYPE_REVISE_EXISTING), 
                isProposalAdoptInternationalPublications: (this.proposalType == this.constants.PROJECT_PROPOSAL_TYPE_ADOPT_INTERNATIONAL), 
                adoptionIdenticalOrModifyContent: (this.proposalType != null && this.proposalType != ''), 
                isAdoptionModifiedContent: (this.adoptionIdenticalOrModifyContent == this.constants.PROJECT_TYPE_CHOICES_MODIFIED), 
                projectLabel: 'Project #' + (this.projectIndex) + ': ' + projectRecord.Project_Type__c     
            });
        }

        console.log('~~ this.projects', this.projects);
    }

    saveProposal(isDraft, callback) {
        //this.proposalRecord.Id = this.proposalId;

        let proposalRecord = JSON.parse(JSON.stringify(this.proposalRecord)); 

        proposalRecord.Id = this.proposalId;

        if (isDraft === true) {
            proposalRecord.Proposal_Status__c = this.constants.PROPOSAL_STATUS_DRAFT;
            if (!this.reviewAllProposedProjectsScreen) {
                this.upsertProjectToMap();
            }
        } else {
            proposalRecord.Proposal_Status__c = this.constants.PROPOSAL_STATUS_NEW;
        }
        
        this.proposalRecord = Object.assign({}, proposalRecord);

        upsertProposalAndProjects({
            proposalRecord: this.proposalRecord, 
            projectsWithPublications: this.projects

        }).then(result => {

            console.log('~ result', result);

            if (result) {                                
                this.proposalId = Object.keys(result)[0];

                console.log('~~ returned proposal Id', this.proposalId);

                if (result[this.proposalId]) {
                    this.currentProjectId = result[this.proposalId][result[this.proposalId].length - 1].Id;
                    console.log('~~ this.currentProjectId', this.currentProjectId);
                }
                callback(true);
            } else {
                callback(false);
            }
            
        }).catch(error => {
            error = reduceErrors(error);
            console.log(error);
            this.error = error;

            callback(false);
        });
    }

    handleBtnClick(event) {
        console.log('~~ btn', event.target.value);
    }

    handleCancelProposalButton(event) {
        
    }

    handleSaveDraftProposalButton(event) {
        let me = this;
        me.isRendering = true;

        me.saveProposal(true, function() {
            me.isRendering = false;
        });
    }

    handleSaveProposalButton(event) {
        let me = this;
        me.isRendering = true;

        me.saveProposal(false, function() {
            me.isRendering = false;
        });
    }

    handleNextButton(event) {
        if (this.proposalScreen === true) {
            if (this.proposalType == this.constants.PROJECT_PROPOSAL_TYPE_CREATE_NEW) {
                this.proposalScreen = false;
                this.createNewPublicationScreen1 = true;
                this.createNewPublicationScreen2 = false;
                this.amendExistingPublicationsScreen1 = false;
                this.amendExistingPublicationsScreen2 = false;
                this.adoptInternationalPublicationsScreen1 = false;
                this.adoptInternationalPublicationsScreen2 = false;
                this.addMoreProposalScreen = false;
                this.netBenefitScreen1 = false;
                this.netBenefitScreen2 = false;
                this.reviewAllProposedProjectsScreen = false;
                this.declarationAndPrivacyNoticeScreen = false;
                this.thankYouScreen = false;

            } else if (this.proposalType == this.constants.PROJECT_PROPOSAL_TYPE_REVISE_EXISTING) {
                this.proposalScreen = false;
                this.createNewPublicationScreen1 = false;
                this.createNewPublicationScreen2 = false;
                this.amendExistingPublicationsScreen1 = true;
                this.amendExistingPublicationsScreen2 = false;
                this.adoptInternationalPublicationsScreen1 = false;
                this.adoptInternationalPublicationsScreen2 = false;
                this.addMoreProposalScreen = false;
                this.netBenefitScreen1 = false;
                this.netBenefitScreen2 = false;
                this.reviewAllProposedProjectsScreen = false;
                this.declarationAndPrivacyNoticeScreen = false;
                this.thankYouScreen = false;

            } else if (this.proposalType == this.constants.PROJECT_PROPOSAL_TYPE_ADOPT_INTERNATIONAL) {
                this.proposalScreen = false;
                this.createNewPublicationScreen1 = false;
                this.createNewPublicationScreen2 = false;
                this.amendExistingPublicationsScreen1 = false;
                this.amendExistingPublicationsScreen2 = false;
                this.adoptInternationalPublicationsScreen1 = true;
                this.adoptInternationalPublicationsScreen2 = false;
                this.addMoreProposalScreen = false;
                this.netBenefitScreen1 = false;
                this.netBenefitScreen2 = false;
                this.reviewAllProposedProjectsScreen = false;
                this.declarationAndPrivacyNoticeScreen = false;
                this.thankYouScreen = false;
            }

        } else if (this.createNewPublicationScreen1 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = true;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;

        } else if (this.createNewPublicationScreen2 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = true;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;

        } else if (this.amendExistingPublicationsScreen1 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = true;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.amendExistingPublicationsScreen2 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = true;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.adoptInternationalPublicationsScreen1 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = true;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.adoptInternationalPublicationsScreen2 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = true;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.addMoreProposalScreen === true) {
            this.upsertProjectToMap();

            if (this.willAddMoreProjects == 'Yes') {            
                this.proposalScreen = true;
                this.createNewPublicationScreen1 = false;
                this.createNewPublicationScreen2 = false;
                this.amendExistingPublicationsScreen1 = false;
                this.amendExistingPublicationsScreen2 = false;
                this.adoptInternationalPublicationsScreen1 = false;
                this.adoptInternationalPublicationsScreen2 = false;
                this.addMoreProposalScreen = false;
                this.netBenefitScreen1 = false;
                this.netBenefitScreen2 = false;
                this.reviewAllProposedProjectsScreen = false;
                this.declarationAndPrivacyNoticeScreen = false;
                this.thankYouScreen = false;

                this.projectIndex ++;
                this.initializeValues();
                this.isRendering = false;

            } else if (this.willAddMoreProjects == 'No') {
                this.proposalScreen = false;
                this.createNewPublicationScreen1 = false;
                this.createNewPublicationScreen2 = false;
                this.amendExistingPublicationsScreen1 = false;
                this.amendExistingPublicationsScreen2 = false;
                this.adoptInternationalPublicationsScreen1 = false;
                this.adoptInternationalPublicationsScreen2 = false;
                this.addMoreProposalScreen = false;
                this.netBenefitScreen1 = true;
                this.netBenefitScreen2 = false;
                this.reviewAllProposedProjectsScreen = false;
                this.declarationAndPrivacyNoticeScreen = false;
                this.thankYouScreen = false;
            }

        } else if (this.netBenefitScreen1 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = true;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;

        } else if (this.netBenefitScreen2 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = true;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;

        } else if (this.reviewAllProposedProjectsScreen === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = true;
            this.thankYouScreen = false;
        }
    }

    handleSubmitProposalButton(event) {
        if (this.declarationAndPrivacyNoticeScreen === true) {
            if (this.disclaimerCheck == 'Agree') {
                let me = this;
                me.isRendering = true;

                me.saveProposal(false, function(isSuccess) {                    
                    
                    if (isSuccess) {
                        me.proposalScreen = false;
                        me.createNewPublicationScreen1 = false;
                        me.createNewPublicationScreen2 = false;
                        me.amendExistingPublicationsScreen1 = false;
                        me.amendExistingPublicationsScreen2 = false;
                        me.adoptInternationalPublicationsScreen1 = false;
                        me.adoptInternationalPublicationsScreen2 = false;
                        me.addMoreProposalScreen = false;
                        me.netBenefitScreen1 = false;
                        me.netBenefitScreen2 = false;
                        me.reviewAllProposedProjectsScreen = false;
                        me.declarationAndPrivacyNoticeScreen = false;
                        me.thankYouScreen = true;

                        me.initializeValues();
                        me.projects = [];
                    }
                    me.isRendering = false;
                });
            }
        }
    }

    get hasRelatedAnyExistingPublications() {
        return (this.isRelatedToAnyExistingPublications == 'Yes');
    }

    get isAdoptionModifiedContent() {
        return (this.adoptionIdenticalOrModifyContent == this.constants.PROJECT_TYPE_CHOICES_MODIFIED);
    }

    get isDisclaimerChecked() {
        return (this.disclaimerCheck == 'Agree');
    }

    get isDisclaimerDisagree() {
        return (this.disclaimerCheck == 'Disagree' );
    }

    get isProposalCreateNewPublication() {
        return (this.proposalType == this.constants.PROJECT_PROPOSAL_TYPE_CREATE_NEW);
    }

    get isProposalAmendingExistingPublications() {
        return (this.proposalType == this.constants.PROJECT_PROPOSAL_TYPE_REVISE_EXISTING);
    }

    get isProposalAdoptInternationalPublications() {
        return (this.proposalType == this.constants.PROJECT_PROPOSAL_TYPE_ADOPT_INTERNATIONAL);
    }

    get hideNextButton() {
        return this.declarationAndPrivacyNoticeScreen 
            || (this.proposalRecord.Proposal_Status__c == this.constants.PROPOSAL_STATUS_NEW && this.reviewAllProposedProjectsScreen);
    }

    get showSaveDraftButton() {
        return (!this.proposalRecord.Proposal_Status__c || this.proposalRecord.Proposal_Status__c == this.constants.PROPOSAL_STATUS_DRAFT);
    }

    errorCallback(error, stack) {
        this.error = reduceErrors(error);

        console.log('~~ ERROR: errorCallback -> this.error -> ', this.error);
        console.log('~~ ERROR: errorCallback -> stack -> ', stack);
    }

    handleProjectUpdateEvent(event) {
        try {
            let updatedProject = event.detail.project;
            let tempProjects = Object.assign([], this.projects);

            tempProjects.forEach(function(obj, index) {
                if (updatedProject.index === obj.index){
                    this[index] = updatedProject;
                }
                
            }, tempProjects);

            this.projects = Object.assign([], tempProjects);
        
        } catch(error) {
            console.log(error);
        }            
    }

    handleProposalUpdateEvent(event) {
        try {        
            this.proposalRecord = Object.assign({}, event.detail.proposalRecord);

        } catch (error) {
            console.log(error);
        }            
    }

}