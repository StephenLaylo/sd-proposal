/**
    @author:            Stephen Laylo
    @company:           J4RVIS
    @description:       JS Controller for FileUpload LWC that handles the File Upload form
**/

import { LightningElement, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';
import getRelatedFiles from '@salesforce/apex/FileUploadController.getRelatedFiles';
import { reduceErrors } from 'c/ldsUtils';

export default class FileUpload extends LightningElement {

    @api recordId;
    @api cardTitle;
    @api cardIconName;
    @api fileUploadLabel;
    @api viewOnly;

    get acceptedFormats() {
        return [
            '.doc', 
            '.docx', 
            '.pdf', 
            '.ppt', 
            '.pptx', 
            '.xls', 
            '.xlsx', 
            '.gif', 
            '.jpg', 
            '.jpeg', 
            '.png', 
            '.xltm', 
            '.msg', 
            '.txt', 
            '.zip', 
            '.rar', 
            '.7z'
        ];
    }

    @wire(getRelatedFiles, { recordId: '$recordId' })
    files;

    handleUploadFinished(event) {
        const uploadedFiles = event.detail.files;
        
        refreshApex(this.files);

        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Success',
                message: uploadedFiles.length + ' File(s) uploaded successfully!',
                variant: 'success',
            }),
        );
    }

    handleDeleteFinished(event) {
        refreshApex(this.files);
    }

    // handleAddFilesButtonClick(event) {
    //     const fileUploadInput = this.template.querySelector('.file-upload > input');

    //     console.log('~ fileUploadInput -> ', fileUploadInput);

    //     if (fileUploadInput) {
    //         fileUploadInput.click();
    //     }
    // }

    refreshFiles(event) {
        refreshApex(this.files);
    }

}