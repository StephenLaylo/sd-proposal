import { LightningElement, api, track } from 'lwc';
import getProposalRecord from '@salesforce/apex/ProposalFormController.getProposalRecord';
import PROPOSAL_OBJECT from '@salesforce/schema/Proposal__c';

export default class ProposalDetails extends LightningElement {
    
    @api constants;
    @api proposalId;
    @api proposalRecord;

    @track proposalWrapper = {
        proposalRecord: {
            apiName: PROPOSAL_OBJECT.objectApiName
        },
        projects: []
    }

    activeSections = ['Proposal_Details', 'Projects', 'Net_Benefits', 'Files'];
    
    connectedCallback() {
        if (this.proposalId) {
            getProposalRecord({
                proposalId: this.proposalId

            }).then(result => {               
    
                if (result) {
                    this.proposalWrapper = {
                        proposalRecord: null,
                        projects: []
                    }
            
                    console.log(JSON.stringify(result));
                    
                    this.proposalWrapper.proposalRecord = result.proposalRecord;
            
                    if (result.projects) {
                        result.projects.map(project => {
                            this.proposalWrapper.projects.push(
                                {
                                    projectRecord: project.projectRecord,
                                    selectedPublications: project.selectedPublications,
                                    isNew: project.projectRecord.Project_Type__c == this.constants.PROJECT_TYPE_NEW,
                                    isReviseAmend: project.projectRecord.Project_Type__c == this.constants.PROJECT_TYPE_REVISED_TEXT_AMENDMENT,
                                    isAdoptionIdentical: project.projectRecord.Project_Type__c == this.constants.PROJECT_TYPE_INTERNATIONAL_ADOPTION_IDENTICAL,
                                    isAdoptionModified: project.projectRecord.Project_Type__c == this.constants.PROJECT_TYPE_INTERNATIONAL_ADOPTION_MODIFIED,
                                    showTitleDesignation: (project.projectRecord.Project_Type__c == this.constants.PROJECT_TYPE_REVISED_TEXT_AMENDMENT && project.projectRecord.Existing_Publication_to_revise_amend__c == null) 
                                                            || ((project.projectRecord.Project_Type__c == this.constants.PROJECT_TYPE_INTERNATIONAL_ADOPTION_IDENTICAL || project.projectRecord.Project_Type__c == this.constants.PROJECT_TYPE_INTERNATIONAL_ADOPTION_MODIFIED) 
                                                                && project.projectRecord.International_Publication_to_adopt__c == null)                                            
                                }
                            );
                        });
                    }
                }
                
            }).catch(error => {
                console.log(error);
            });
        }
    }
    
    handleBackToProposalList(event) {
        const evtCustomEvent = new CustomEvent('close', { 
            detail: null
        });

        this.dispatchEvent(evtCustomEvent);
    }

    handleEdit(event) {
        const evtCustomEvent = new CustomEvent('togglecontinuedraftproposal', 
            {                    
                bubbles: true,
                composed: true,
                detail: {
                    proposalWrapper: this.proposalWrapper
                }
            }
        );

        this.dispatchEvent(evtCustomEvent);        
    }

    get isDraftProposal() {
        return this.proposalWrapper.proposalRecord.Proposal_Status__c == this.constants.PROPOSAL_STATUS_DRAFT || 
               this.proposalWrapper.proposalRecord.Proposal_Status__c == this.constants.PROPOSAL_STATUS_NEW || 
               this.proposalWrapper.proposalRecord.Proposal_Status__c == this.constants.PROPOSAL_STATUS_IN_DEVELOPMENT;
    }

    get canUserView() {
        return !this.isDraftProposal;
    }

}