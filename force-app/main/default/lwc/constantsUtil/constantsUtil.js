import getAllConstants from '@salesforce/apex/Constants.getAllConstants';

export async function getConstantValues() {
    return await getAllConstants().then(data => {
        console.log('~~ constantsUtil -> getConstantValues -> getAllConstants -> results : ', data);

        return data;

    }).catch(error => {
        console.log('~~ constantsUtil -> getConstantValues -> getAllConstants -> error : ', error);

        return null;
    });
}