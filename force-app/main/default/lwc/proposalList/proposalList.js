import { LightningElement, wire, track, api } from 'lwc';
import { reduceErrors } from 'c/ldsUtils';
import getProposalsBySearchText from '@salesforce/apex/ProposalFormController.getProposalsBySearchText'
import getPicklistValues from '@salesforce/apex/CommonUtils.getPicklistValues'

const proposalColumns = [
    { 
        label: 'Proposer\'s Name', 
        fieldName: 'ProposerContactName', 
        type: 'text', 
        wrapText: true
    }, 
    { 
        label: 'Proposal Name', 
        fieldName: 'Name', 
        type: 'text', 
        sortable: true, 
        wrapText: true
    }, 
    { 
        label: 'Proposal Status', 
        fieldName: 'Proposal_Status__c', 
        type: 'text', 
        sortable: true, 
        wrapText: true
    }, 
    { 
        label: 'Submitted Date', 
        fieldName: 'Submitted_Date__c', 
        type: 'date', 
        sortable: true, 
        wrapText: true
    }, 
    { 
        label: 'Consultation Start Date', 
        fieldName: 'Consultation_Start_Date__c', 
        type: 'date', 
        sortable: true, 
        wrapText: true
    }, 
    { 
        label: 'Consultation End Date', 
        fieldName: 'Consultation_End_Date__c', 
        type: 'date', 
        sortable: true, 
        wrapText: true
    }, 
    { 
        label: 'Approved Date', 
        fieldName: 'Approved_Date__c', 
        type: 'date', 
        sortable: true, 
        wrapText: true
    }, 
    { 
        label: 'Completed Date', 
        fieldName: 'Complete_Date__c', 
        type: 'date', 
        sortable: true, 
        wrapText: true
    }, 
    { 
        label: 'Action', 
        fieldName: 'ProposalRecord', 
        type: 'button', 
        wrapText: true, 
        typeAttributes: { 
            label: 'View',
            name: 'view'
        }, 
        cellAttributes: {
            class: 'cell-action'
        } 
    }
];

export default class ProposalList extends LightningElement {

    @api constants;

    @track proposalStatus;
    @track isDatatableProcessing = false;
    @track selectedProposalId;
    @track searchText;

    @track pageNumber = 1;
    @track rowsPerPage = '5';
    @track sortedField = 'LastModifiedDate';
    @track sortDirection = 'DESC';

    proposalColumns = proposalColumns;

    rowsPerPageChoices = {
        data: {
            values: [
                {
                    label: '5',
                    value: '5'
                },
                {
                    label: '10',
                    value: '10'
                },
                {
                    label: '20',
                    value: '20'
                },
                {
                    label: '50',
                    value: '50'
                },
                {
                    label: '100',
                    value: '100'
                },
                {
                    label: '200',
                    value: '200'
                },
            ]
        }
    };

    @track proposalsData;
    @track proposalStage;
    @track proposalStatusValues;

    connectedCallback() {
        this.proposalStageValues = {
            data: (!this.constants) ? null : {
                values: [
                    {
                        label: this.constants.PROPOSAL_STAGE_FILTER_ALL,
                        value: this.constants.PROPOSAL_STAGE_FILTER_ALL
                    },
                    {
                        label: this.constants.PROPOSAL_STAGE_FILTER_DRAFT,
                        value: this.constants.PROPOSAL_STAGE_FILTER_DRAFT
                    },
                    {
                        label: this.constants.PROPOSAL_STAGE_FILTER_SUBMITTED,
                        value: this.constants.PROPOSAL_STAGE_FILTER_SUBMITTED
                    },
                    {
                        label: this.constants.PROPOSAL_STAGE_FILTER_CONSULTING,
                        value: this.constants.PROPOSAL_STAGE_FILTER_CONSULTING
                    },
                    {
                        label: this.constants.PROPOSAL_STAGE_FILTER_FOR_APPROVAL,
                        value: this.constants.PROPOSAL_STAGE_FILTER_FOR_APPROVAL
                    },
                    {
                        label: this.constants.PROPOSAL_STAGE_FILTER_FINISHED,
                        value: this.constants.PROPOSAL_STAGE_FILTER_FINISHED
                    }
                ]
            }
        };

        this.isDatatableProcessing = true;

        getPicklistValues({ 
            objectName: 'Proposal__c',
            fieldName : 'Proposal_Status__c' 

        }).then(results => {
            results = JSON.parse(JSON.stringify(results));

            let data = [];

            for (let x = 0; x < Object.keys(results).length; x ++) {
                let k = Object.keys(results)[x];
                data.push({
                    value : k, 
                    label : results[k]
                });
            }

            data = { values: data }; 

            this.proposalStatusValues = { 
                data: data
            };

            this.error = null;
            this.isDatatableProcessing = false;

            this.proposalStage = this.constants.PROPOSAL_STAGE_FILTER_ALL;
            this.handleProposalStageChange(null);
            this.initializeProposalList();
            
        }).catch(error => {
            this.proposalStatusValues = null;
            this.error = reduceErrors(error);
            this.isDatatableProcessing = false;
        });
    }

    handleSelectRow(event) {
        const proposal = event.detail.row;

        this.selectedProposalId = proposal.Id;
    }

    handleCloseProposalDetails(event) {
        this.selectedProposalId = null;
        this.proposalStage = this.constants.PROPOSAL_STAGE_FILTER_ALL;

        const evtCustomEvent = new CustomEvent('close', { 
            detail: null
        });

        this.dispatchEvent(evtCustomEvent);
    }

    handleRowsPerPageChange(event) {
        this.pageNumber = 1;
        this.rowsPerPage = event.detail.value;

        this.initializeProposalList();
    }

    handleProposalStageChange(event) {
        if (event) {
            this.proposalStage = event.detail.value;
        }

        this.proposalStatus = [];
        this.pageNumber = 1;

        if (this.proposalStage == this.constants.PROPOSAL_STAGE_FILTER_DRAFT) {
            this.proposalStatus = [
                this.constants.PROPOSAL_STATUS_DRAFT
            ];

        } else if (this.proposalStage == this.constants.PROPOSAL_STAGE_FILTER_SUBMITTED) {
            this.proposalStatus = [
                this.constants.PROPOSAL_STATUS_NEW, 
                this.constants.PROPOSAL_STATUS_IN_DEVELOPMENT
            ];

        } else if (this.proposalStage == this.constants.PROPOSAL_STAGE_FILTER_CONSULTING) {
            this.proposalStatus = [
                this.constants.PROPOSAL_STATUS_IN_CONSULTATION, 
                this.constants.PROPOSAL_STATUS_CONSULTATION_CLOSED
            ];

        } else if (this.proposalStage == this.constants.PROPOSAL_STAGE_FILTER_FOR_APPROVAL) {
            this.proposalStatus = [
                this.constants.PROPOSAL_STATUS_HSE_REVIEW, 
                this.constants.PROPOSAL_STATUS_PMG_REVIEW, 
                this.constants.PROPOSAL_STATUS_SDAC_REVIEW, 
                this.constants.PROPOSAL_STATUS_AWAITING_RESOURCES
            ];

        } else if (this.proposalStage == this.constants.PROPOSAL_STAGE_FILTER_FINISHED) {
            this.proposalStatus = [
                this.constants.PROPOSAL_STATUS_APPROVED, 
                this.constants.PROPOSAL_STATUS_COMPLETED, 
                this.constants.PROPOSAL_STATUS_NOT_PROGRESSED
            ];

        } else {
            if (this.proposalStatusValues.data != null) {
                if (this.proposalStatusValues.data.values != null && this.proposalStatusValues.data.values.length > 0) {

                    for (let i = 0; i < this.proposalStatusValues.data.values.length; i ++) {
                        this.proposalStatus.push(this.proposalStatusValues.data.values[i].value);
                    }
                }
            }
        }

        this.initializeProposalList();
    }

    handleSearchTextOnChange(event) {
        this.pageNumber = 1;
        this.searchText = event.detail.value;

        this.initializeProposalList();
    }

    handleSort(event) {
        this.pageNumber = 1;
        this.sortedField = event.detail.fieldName;
        this.sortDirection = event.detail.sortDirection;

        this.initializeProposalList();
    }

    initializeProposalList() {
        this.isDatatableProcessing = true;
        this.proposalsData = null;

        getProposalsBySearchText({
            proposalStatus: this.proposalStatus, 
            searchText: this.searchText,
            pageNumber: this.pageNumber, 
            rowsPerPage: parseInt(this.rowsPerPage), 
            sortedField: this.sortedField, 
            sortDirection: this.sortDirection

        }).then(results => {
            if (results != null && results.length > 0) {
                for (let i = 0; i < results.length; i ++) {
                    results[i].ProposerContactName = (results[i].Proposer_Contact__r ? results[i].Proposer_Contact__r.Name : '');
                    results[i].ProposalRecord = JSON.stringify(results[i]);
                }

                this.proposalsData = results;
            }

            this.isDatatableProcessing = false;

        }).catch(error => {
            this.error = reduceErrors(error);

            this.isDatatableProcessing = false;
        });
    }

}