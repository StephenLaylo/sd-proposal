import { LightningElement, api } from 'lwc';

export default class FileDetails extends LightningElement {

    @api file;
    @api edit = false;

    handleBackToFileList() {
        const evtCustomEvent = new CustomEvent('close');

        this.dispatchEvent(evtCustomEvent);
    }

}