import { LightningElement, api, track } from 'lwc';
import sdProposalResources from '@salesforce/resourceUrl/SD_Proposal';
import { getConstantValues } from 'c/constantsUtil';

export default class ProposalDashboard extends LightningElement {

    /* VARIABLES FOR CONFIGURATIONS FOR LABELS */
    @api proposalQuestion; // = 'What are you wanting to propose?';
    @api publicationTypeQuestion; // = 'What type of publication are you proposing?';
    @api relatedToAnyExistingPublicationsQuestion; // = 'Does your proposal relate to any existing publications?';
    @api multipleRelatedPublicationsSearchQuestion; //= 'Please select the publications that are related to your proposal';
    @api proposedPublicationTitleQuestion; // = 'What will the title of your proposed publication be?';
    @api problemDesignedToSolveQuestion; // = 'What problem is this designed to solve?';
    @api proposedPublicationScopeQuestion; // = 'What is the scope of the new publication? (be as detailed & comprehensive as possible)';
    @api revisePublicationsSearchQuestion; // = 'Search for publication you want to revise/amend';
    @api revisePublicationDesignationTitleQuestion; // = 'Enter Designation & Title of publication you want to revise/amend';
    @api revisedPublicationDesignationQuestion; // = 'Designation';
    @api revisedPublicationTitleQuestion; // = 'Title';
    @api currentEditionIssuesQuestion; // = 'What are the issues with the current edition?';
    @api evidenceExistsProblemQuestion; // = 'What evidence exists to demonstrate there is a problem?';
    @api proposedSolutionQuestion; // = 'What is your proposed solution (be as detailed & comprehensive as possible)?';
    @api multipleInternationalPublicationsSearchQuestion; // = 'Search for international publication you want to adopt';
    @api adoptInternationalStandardDesignationTitleQuestion; // = 'Enter Designation & Title of International Standard you want to adopt';
    @api adoptedPublicationDesignationQuestion; // = 'Designation';
    @api adoptedPublicationTitleQuestion; // = 'Title';
    @api adoptionIdenticalOrModifyContentQuestion; // = 'Is this adoption identical, or do you need to modify the content?';
    @api whyPublicationBeAdoptedInAustraliaQuestion; // = 'Why should this publication be adopted in Australia?';
    @api whyCannotAdoptedIdenticallyCurrentInternationalStandardQuestion; // = 'Why can’t the current international standard be adopted identically (refer to TBT agreement information)';
    @api proposedVariationsQuestion; // = 'What are your proposed variations (be as detailed as possible)';
    @api addMoreProjectsQuestion; // = 'Do you want to add more projects to your proposal?';
    @api benefitsToAustralianCommunityPublicHealthAndSafetyQuestion; // = 'How will this work bring Net Benefit to the Australian Community - Public and health safety?';
    @api benefitsToAustralianCommunitySocialAndCommunityImpactQuestion; // = 'How will this work bring Net Benefit to the Australian Community - Social and community impact?';
    @api benefitsToAustralianCommunityEnvironmentalImpactQuestion; // = 'How will this work bring Net Benefit to the Australian Community - Environmental impact?';
    @api benefitsToAustralianCommunityCompetitionQuestion; // = 'How will this work bring Net Benefit to the Australian Community - Competition?';
    @api benefitsToAustralianCommunityEconomicImpactQuestion; // = 'How will this work bring Net Benefit to the Australian Community - Economic impact?';
    @api consultedPeopleQuestion; // = 'Who has been consulted and what are their views?';
    @api declarationText; // = 'Review and agree to Declaration (including Privacy Notice)';
    @api doNotAgreeText; // = 'You are required to agree with the declaration terms before you can submit this proposal. If you do not agree to the terms please contact a SEM to discuss: www.standards.org.au/engagementevents/ sectors';
    @api thankYouText; // = 'Thank you for submitting your proposal';
    @api noMatchFoundText; // = 'No Match Found. Please search again, or enter the designation and title below.';

    @track sectionHeader = null;
    @track isCreateNewProposalSelected = false;
    @track isMyProposalsSelected = false;
    @track isLoading = false;

    @track constants;

    continueDraft = false;
    proposalRecord = [];
    projects = [];

    isoIcon = sdProposalResources + '/images/iso.png';
    simIcon = sdProposalResources + '/images/sim.png';

    connectedCallback() {
        let me = this;
        
        this.constants = getConstantValues().then(results => {
            me.constants = JSON.parse(JSON.stringify(results));
        });
    }

    handleCreateNewProposalButton(event) {
        this.sectionHeader = 'Create New Proposal';
        this.isCreateNewProposalSelected = true;
        this.isMyProposalsSelected = false;

        let dealProposalComponent = this.template.querySelectorAll('c-deal-proposal');

        if (dealProposalComponent) {
            if (dealProposalComponent.length > 0) {
                dealProposalComponent[0].refreshComponent();
            }
        }

        let me = this;

        setTimeout(function() {
            me.isCreateNewProposalSelected = true;
        }, 100);
    }

    handleMyProposalsButton(event) {
        this.sectionHeader = 'My Proposals';

        this.isCreateNewProposalSelected = false;
        this.isMyProposalsSelected = false;

        let me = this;

        setTimeout(function() {
            me.isMyProposalsSelected = true;
        }, 100);
    }

    handleCloseProposalDetails(event) {
        this.sectionHeader = 'My Proposals';
        this.handleMyProposalsButton(null);
    }

    handleContinueDraftProposal(event) {
        this.continueDraft = true;
        this.sectionHeader = 'Resume Proposal';
        this.isMyProposalsSelected = false;
        this.isCreateNewProposalSelected = true;
        this.proposalRecord = event.detail.proposalWrapper.proposalRecord;        

        if (event.detail.proposalWrapper.projects) {
            let n = 1;
            let tempProjects = [];
            
            event.detail.proposalWrapper.projects.map(project => {
                tempProjects.push(
                    {
                        projectRecord: project.projectRecord,
                        selectedPublications: project.selectedPublications,
                        isProposalCreateNewPublication: project.isNew,
                        isProposalAmendingExistingPublications: project.isReviseAmend,
                        isProposalAdoptInternationalPublications: project.isAdoptionIdentical || project.isAdoptionModified,
                        adoptionIdenticalOrModifyContent: project.isAdoptionIdentical,
                        isAdoptionModifiedContent: project.isAdoptionModified, 
                        projectLabel: 'Project #' + (n) + ': ' + project.projectRecord.Project_Type__c                                   
                    }
                );

                n ++;
            });

            this.projects = tempProjects;
        }        
    }
}